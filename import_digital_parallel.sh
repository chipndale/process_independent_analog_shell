#!/bin/sh
#############################################################################
# This is a design imporrt script from digital flow to Cadence Virtuoso 
# design environment.
# The purpose is to generate all needed files
# in parametrized manner from this single file
# Created by Marko Kosunen on 24.03.2015
# Last modification by Marko Kosunen, marko.kosunen@aalto.fi, 10.12.2018 10:02
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f() {
cat << EOF
IMPORT_DIGITAL_PARALLEL Release 1.0 (02.12.2018)
import_digital_parallel-import the desing from digital flow 
in parallel processes with LSF
Written by Marko Pikkis Kosunen

SYNOPSIS
  import_digital_parallel [OPTIONS] 
DESCRIPTION
  Producess all configuration and Makefile for the digital flow

OPTIONS
  -c
      Top cell name.
  -g
      GDS file name.
  -l
      Virtuoso library name.
  -r
      Reference library list "STRING".
  -s
      Font scaling factor.
  -t
      Technology library.
  -v
      VERILOG netlist.
  -h
      Show this help.
EOF
}
CELL=""
FORCE=""
VIRTUOSOTARGETLIB=""
QUEUELIST=""
REFLIBS=""
NETLIST=""
FORCE=""
TECHLIB=""
FONTSCALE="1"
while getopts c:fg:l:q:r:s:t:v:h opt
do
  case "$opt" in
    c) CELL=${OPTARG};; 
    f) FORCE="-f ";;
    g) GDS=${OPTARG};;
    l) VIRTUOSOTARGETLIB=${OPTARG};;
    q) QUEUELIST=${OPTARG};;
    r) REFLIBS=${OPTARG};;
    s) FONTSCALE=${OPTARG};;
    t) TECHLIB=${OPTARG};;
    v) NETLIST=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done
if [ -z "$TECHLIB" ]; then
    echo "Technology library not given"
    help_f;
    exit 0
fi

if [ -z "${QUEUELIST}" ]; then
    QUEUEARG=""
else
    QUEUEARG="-q\"${QUEUELIST} \" "
fi
echo "Queuearg is ${QUEUEARG}"

#DEPJOBS=""
#for macro in $MACROLIBS; do
#    QUEUE=$(./shell/findqueue.sh -q"normal bora")
#    echo "Submitting import_${macro} to $QUEUE"
#    bsub -J "import_$macro" -q$QUEUE ${WORKDIR}/shell/import_macro_package.sh -w ${WORKDIR} -f -p${MACRODIR}/virtuoso/${macro}.tbz2 
#    if [ -z "$DEPJOBS" ]; then
#        DEPJOBS="done(import_${macro})"
#    else
#        DEPJOBS="$DEPJOBS && done(import_${macro})"
#    fi
#
#done
#echo "Depjobs is $DEPJOBS"
#QUEUE=$(./shell/findqueue.sh -q"normal bora")
#bsub -w "$DEPJOBS" -q $QUEUE touch ./import_analog.tag
#cd ..

#DEPJOBS=""
#cd ${WORKDIR}
#for mem in $MEMLIBS; do
#    memlower=$(echo $mem | tr '[:upper:]' '[:lower:]')
#    echo "Memlower is ${memlower}"
#    QUEUE=$(./shell/findqueue.sh -q"normal bora")
#    echo "Submitting import_${mem} to $QUEUE"
#    bsub -J "import_${mem}" -q$QUEUE \
#    ./shell/digi2virtuoso.sh -c ${mem} -f \
#    -g ${INNOVUSDIR}/tech/memories/${memlower}_????/GDSII/${memlower}_????_m4xdh.gds  \
#-l $mem -r basic \
#-v ${INNOVUSDIR}/tech/memories/${memlower}_????/VERILOG/${memlower}_????_pwr.v \
#-t yourLocalTechLibrary \
#-s 15 -p
#
#    QUEUE=$(./shell/findqueue.sh -q"normal bora")
#    bsub -J "sortpin_${mem}" \ 
#    ./shell/subckt_sortpins.sh -s ${INNOVUSDIR}/tech/memories/${memlower}_????/SPICE/${memlower}_????.spi -d ${SPICEINCLUDEDIR}/${memlower}_????.spi -S ${mem}
#
#    if [ -z "$DEPJOBS" ]; then
#        DEPJOBS="done(import_${mem}) && done(sortpin_${mem})"
#    else
#        DEPJOBS="$DEPJOBS && done(import_${mem}) && done(sortpin_${mem})"
#    fi
#done
#
#echo "Depjobs is $DEPJOBS"
#QUEUE=$(./shell/findqueue.sh -q"normal bora")
#bsub -w "$DEPJOBS" -q $QUEUE touch ./import_memories.tag
#cd ..

touch ./empty_submodules.txt
rm -f ./ignore_submodules.txt
MODULES=$(sed -n '/^module/p' ${NETLIST} | sed 's/\(^\s*module \)\(.*\)\( .*$\)/\2/g' | xargs)

# This only creates the library and symbols
QUEUE=$(./shell/findqueue.sh -q"${QUEUELIST}")
echo "Submitting pre_import_${CELL} to $QUEUE"
bsub -J "pre_import_${CELL}" -q $QUEUE \
    -oo ./pre_import_${CELL}.log -eo ./pre_import_${CELL}.err \
    ./shell/digi2virtuoso.sh -c ${CELL} ${FORCE} \
    -i ./empty_submodules.txt \
    -l ${VIRTUOSOTARGETLIB} -r "${REFLIBS} ${VIRTUOSOTARGETLIB}" \
    -v ${NETLIST} \
    -t "${TECHLIB}" \
    -s 15

#Then the layout only 
DEPJOBS=""
QUEUE=$(./shell/findqueue.sh -q"${QUEUELIST}")
echo "Submitting import_${CELL} to $QUEUE"
bsub -w"done(pre_import_${CELL})" -J "import_${CELL}" -q $QUEUE \
    -oo ./import_${CELL}.log -eo ./import_${CELL}.err \
./shell/digi2virtuoso.sh -c ${CELL} \
-g ${GDS} \
-l ${VIRTUOSOTARGETLIB} -r "${REFLIBS} ${VIRTUOSOTARGETLIB}" \
-t "${TECHLIB}" \
-s 15 -o -O

if [ -z "$DEPJOBS" ]; then
    DEPJOBS="done(import_${CELL})"
else
    DEPJOBS="$DEPJOBS && done(import_${CELL})"
fi

###Read in the subcells, do not overwrite symbols
SMFILES=""
for cell in $MODULES; do
    QUEUE=$(./shell/findqueue.sh -q"${QUEUELIST}")
    echo $cell > ./${cell}_submodules.txt 
    echo "Submitting import_${cell} to $QUEUE"
    bsub -w"done(pre_import_${CELL})" -J "import_${cell}" -q $QUEUE \
    -oo ./import_${cell}.log -eo ./import_${cell}.err \
    ./shell/digi2virtuoso.sh -c ${cell} \
    -i ./${cell}_submodules.txt \
    -l ${VIRTUOSOTARGETLIB} -r "${REFLIBS}" \
    -v ${NETLIST} \
    -t "${TECHLIB}" \
    -s 15 -o -O
    if [ -z "$DEPJOBS" ]; then
        DEPJOBS="done(import_${cell})"
        SMFILES="${cell}_submodules.txt"
    else
        DEPJOBS="$DEPJOBS && done(import_${cell})"
        SMFILES="${CELLS} ${cell}_submodules.txt"
    fi
    sleep 2
done

# Then run the fix round. This will import sequentially everything that 
# for some reason is not imported during the previous step
# Wait until subcells are done
QUEUE=$(./shell/findqueue.sh -q"${QUEUELIST}")
bsub -w "$DEPJOBS" -J "import_digital_${CELL}_fix" -K -q $QUEUE \
    -oo ./digital_import_done.log -eo ./digital_import_done.err \
    ./shell/digi2virtuoso.sh -c ${CELL} \
    -l ${VIRTUOSOTARGETLIB} -r "${REFLIBS}" \
    -v ${NETLIST} \
    -t "${TECHLIB}" \
    -s 15 -O
    sleep 2
    rm -f ${SMFILES} && touch import_digital.tag 
cd ..
exit 0

