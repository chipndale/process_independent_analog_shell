#!/bin/sh
#############################################################################
# This script generates a dummy LIB file from an analog circuit layout
# to be used by a digital place-and-route tool (Encounter/Innovus).
# Created by Okko Järvinen on 25.10.2019
#############################################################################
#Function to display help with -h argument and to control
#the configuration from the command line
help_f()
{
SCRIPTNAME="generate_lib"
cat << EOF
${SCRIPTNAME} Release 1.0 (24.10.2019)
Generates dummy LIB from Virtuoso layout.
Written by Okko Järvinen.

SYNOPSIS
    $(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:]) [OPTIONS]
DESCRIPTION
    Generates a dummy LIB-file from LEF file.

OPTIONS
  -c
      Top cell name.   
  -d  
      Target directory. Results will be stored here. Default <working dir>/<Top cell name>_lib
  -f
      Force result overwriting.
  -G 
      Ground net name list. Default is "VSS".
  -l
      Virtuoso library name.
  -O
      Overwrite the requirement of having VSS and VDD as the first entries of supply and ground 
      net lists.
  -S  
      Supply net name list. Default is "VDD".
  -V
      Supply net voltage list. "v1 v2..." To override the default voltage for all supply nets, 
      use -V "0.88" . Otherwise the the length of the list must match the length of the 
      supply net list.- 
  -w 
      Working directory. Must contain cds.lib file. Default: current directory.
  -h
      Show this help.
EOF
}
echo $0
THISDIR=$(dirname $(readlink -f "$0"))
WORKDIR=`pwd`
CELLNAME=""
LIBNAME=""
FORCE="0"
GNDNETS="VSS"
SUPNETS="VDD"
VOLTAGES="0.9"
SUPNETSOVERRIDE="0"

# Input cap and output max drive cap in fF
INPUTCAP=10
MAXCAP=20

while getopts c:d:fG:l:OS:V:w:h opt
do
  case "$opt" in
    c) CELLNAME=${OPTARG};; 
    d) TARGETDIR=${OPTARG};;
    f) FORCE="1";;    
    G) GNDNETS=${OPTARG};;
    l) LIBNAME=${OPTARG};;
    S) SUPNETS=${OPTARG};;
    O) SUPNETSOVERRIDE="1";;
    V) VOLTAGES=${OPTARG};;
    w) WORKDIR=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done
# Generating operating condition strings
. ${THISDIR}/../environment_setup_vars.conf
# The content of the file ${THISDIR}/../environment_setup_vars.conf should contain
#LIBPATH=<PATH TO LIBRARY>
#OPCONDS=""
#for fname in ${LIBPATH}/*.lib.gz; do
#    OPMATCH=$(grep -oP '(<LIBRARY IDENTIFIER STRING>)\K.*' <<< "${fname//${LIBPATH}}")
#    OPCONDS="${OPCONDS} ${OPMATCH//.lib.gz}"
#done
SUPARR=(${SUPNETS})
GNDARR=(${GNDNETS})
VOLTAGESARR=(${VOLTAGES})

if [ "${SUPNETSOVERRIDE}" != "1" ]; then
    if [ "${SUPARR[0]}" != "VDD" ]; then
        cat << EOF  
"Primary supply net is not VDD.  This will most likely break the digital 
flow power domain configuration. Override with option -O if you are sure 
about what you are doing.
EOF
        exit 1
    fi
    if [ "${GNDARR[0]}" != "VSS" ]; then
        cat << EOF  
"Primary ground net is not VSS.  This will most likely break the digital 
flow power domain configuration. Override with option -O if you are sure 
about what you are doing.
EOF
        exit 1
    fi
fi

# Check the lengths of supply and ground arrrays
if [ "${#VOLTAGESARR[@]}" == "${#SUPARR[@]}" ]; then
    echo "Applying voltages:"
    for (( i=0; i < ${#VOLTAGESARR[@]}; i++ )); do
        echo "    ${SUPARR[$i]} = ${VOLTAGESARR[$i]}"
    done
elif [ "${#VOLTAGESARR[@]}" == "1" ]; then
    echo "Applying the given supply voltage for all supply nets:"
    for (( i=0; i < ${#SUPARR[@]}; i++ )); do
        echo $i
        echo "    ${SUPARR[$i]} = ${VOLTAGESARR[0]}"
        VOLTAGESARR[$i]="${VOLTAGESARR[0]}"
    done
else
    echo "ERROR: Multiple supply voltage values given," 
    echo "but the length does not match the length of supply net list"
    help_f
    exit 1
fi

# Create the lef directory
if [ -z "$TARGETDIR" ]; then
    TARGETDIR="${WORKDIR}/${CELLNAME}_lib"
fi
if [ -d "$TARGETDIR" ]; then
    if [ "$FORCE" == "1" ]; then
        rm -rf "$TARGETDIR" 
    else
        echo -e "Directory $TARGETDIR exists.\n\n Define another directory with -d or use -f to force overwrite.\n"
        exit 1;
    fi
fi
mkdir -p $TARGETDIR

if [ -z "$LIBNAME" ]; then
    echo "Virtuoso library not given"
    help_f;
    exit 1
fi
if [ -z "$CELLNAME" ]; then
    echo "Cell not given"
    help_f;
    exit 1
fi

if [ -z "$(echo -n ${OPCONDS} |  sed 's/\s*//g')" ]; then
    echo "OPCONDS not correctly defined in ${THISDIR}/../environment_setup_vars.conf"
    exit 1
fi

INFILE="${WORKDIR}/${CELLNAME}_lef/${CELLNAME}.lef"
if [ ! -f "${INFILE}" ]; then
    echo "Source LEF file not found."
    echo "LEF: ${INFILE}"
    exit 1
fi

VLOGFILE="${WORKDIR}/${CELLNAME}_lef/${CELLNAME}.v"
if [ ! -f "${VLOGFILE}" ]; then
    echo "Source Verilog file not found."
    echo "Verilog: ${VLOGFILE}"
    exit 1
fi

# Calculating the area
SIZEMATCH=$(grep -oP '(SIZE )\K.*' ${INFILE})
SIZEMATCH=${SIZEMATCH// BY}
SIZEMATCH=(${SIZEMATCH// ;})
AREA=$(awk '{print $1*$2}' <<<"${SIZEMATCH[0]} ${SIZEMATCH[1]}")

for op in ${OPCONDS}
do
CURRENTFILE="${TARGETDIR}/${CELLNAME}_${op}.lib"
echo "Generating ${CURRENTFILE}"
cat << EOF > "${CURRENTFILE}"
/*Library definition start*****************************/
library (${CELLNAME}_${op}) {
    technology (cmos);
    time_unit : "1ps";
    voltage_unit : "1V";
    current_unit : "1mA";
    pulling_resistance_unit : "1kohm";
    capacitive_load_unit(1,ff);
    leakage_power_unit : "1uW";
    default_fanout_load : 1;
    simulation : true;
    slew_lower_threshold_pct_rise :  30.00
    slew_upper_threshold_pct_rise :  70.00
    slew_derate_from_library :  0.50
    input_threshold_pct_fall :  50.00
    output_threshold_pct_fall :  50.00 
    input_threshold_pct_rise :  50.00
    output_threshold_pct_rise :  50.00
    slew_lower_threshold_pct_fall :  30.00
    slew_upper_threshold_pct_fall :  70.00    
/*Library definition stop*****************************/

/*Nominal conditions start******************************/
    nom_process : 1;
    nom_voltage : ${VOLTAGESARR[0]};
    nom_temperature : 125;
/*Nominal conditions end********************************/

/*Operating conditions start*****************************/
  operating_conditions("${op}"){
    process : 1;
    temperature : 125;
    Voltage : ${VOLTAGESARR[0]};
    tree_type : "balanced_tree";
  }
  default_operating_conditions : ${op};
/*Operating conditions end*******************************/

/*Voltage map start*************************************/
$( for (( i=0; i < ${#SUPARR[@]}; i++ )); do
    echo "    voltage_map(${SUPARR[$i]}, ${VOLTAGESARR[$i]});"
done )
$( for (( i=0; i < ${#GNDARR[@]}; i++ )); do
    echo "    voltage_map(${GNDARR[$i]}, 0);"
done )
/*Voltage map end***************************************/
EOF
done

PINMATCHES=$(grep -oP '(PIN )\K.*' ${INFILE})
PINMATCHES=(${PINMATCHES})
DIRMATCHES=$(grep -oP '(DIRECTION )\K.*' ${INFILE})
DIRMATCHES=(${DIRMATCHES//;})


BUSNAMES=""
PINNAMES=""
for ((i=0;i<${#PINMATCHES[@]};++i));
do
    PIN=${PINMATCHES[$i]}
    if [[ ${PIN} == *[\[\]]* ]]; then
        BNAME=${PIN%[*}
        if [[ $BUSNAMES != *" $BNAME"* ]]; then
            BUSNAMES="${BUSNAMES} ${BNAME}"
        fi
    else
        # List of pins that are not buses
        if [[ $PINNAMES != *" $PIN"* ]]; then
            PINNAMES="${PINNAMES} ${PIN}"
        fi

    fi
done

CURRENTFILE="${TARGETDIR}/${CELLNAME}_celldef.lib"
cat << EOF > "${CURRENTFILE}"

/*Bus type definitions start*****************************/
EOF

VLOGBUSES=$(grep -oP "(input|output)\K.*\s" ${VLOGFILE})
VLOGBUSARR=(${VLOGBUSES})

VLOGBUSNAMES=$(grep -oP "(input|output) \[\d*:\d*\] \K.*" ${VLOGFILE})
VLOGBUSNAMES=($VLOGBUSNAMES)
for i in "${!VLOGBUSNAMES[@]}"; do
    VLOGBUSNAMES[$i]=${VLOGBUSNAMES[$i]//,}
done

BUSTYPES=""
for bus in $VLOGBUSES
do
    BUSSPLIT=$(echo "${bus}" | tr -d '[')
    BUSSPLIT=$(echo "${BUSSPLIT}" | tr -d ']')
    BUSSPLIT=$(echo "${BUSSPLIT}" | tr ':' ' ')
    BUSSPLIT=(${BUSSPLIT})
    BUSTYPE="bus_${BUSSPLIT[0]}_to_${BUSSPLIT[1]} "
    if [[ $BUSTYPES != *"${BUSTYPE}"* ]]; then
        echo "Created bus type: ${BUSTYPE}"
        BUSTYPES="${BUSTYPES} ${BUSTYPE}"
    else
        BUSTYPES="${BUSTYPES} ${BUSTYPE}"
        continue
    fi
cat << EOF >> "${CURRENTFILE}"
    type(${BUSTYPE}) {
        base_type : array;
        data_type : bit;
        bit_width : $((BUSSPLIT[0]-BUSSPLIT[1]+1));
        bit_from : ${BUSSPLIT[0]};
        bit_to : ${BUSSPLIT[1]};
    }
EOF
done
BUSTYPES=(${BUSTYPES})
cat << EOF >> "${CURRENTFILE}"
/*Bus type definitions end*****************************/

EOF

cat << EOF >> "${CURRENTFILE}"
/*Cell definition start*****************************/
cell (${CELLNAME}) {
    dont_use : true;
    dont_touch : true;
    is_macro_cell : true;
    area : ${AREA};
EOF

for ((i=0;i<${#PINMATCHES[@]};++i));
do
PINDIR=${DIRMATCHES[$i]}
if [ ${PINDIR} == "INOUT" ]; then
    if [[ ${PINMATCHES[$i]} == VDD* ]]; then
        POWSTR="primary_power"
        VDDNAME=${PINMATCHES[$i]}
        POWNAME=${VDDNAME}
    else
        POWSTR="primary_ground"
        VSSNAME=${PINMATCHES[$i]}
        POWNAME=${VSSNAME}
    fi
cat << EOF >> "${CURRENTFILE}"
    pg_pin(${PINMATCHES[$i]}) {
        pg_type : ${POWSTR};
        voltage_name : ${POWNAME};
    }
EOF
else

#Extract pins that are not buses
if [[ ${PINNAMES} == *" ${PINMATCHES[$i]}"* ]]; then
    if [ ${PINDIR} == "INPUT" ]; then
        LOADSTR="capacitance : ${INPUTCAP};"
    else
        LOADSTR="max_capacitance : ${MAXCAP};"
    fi
cat << EOF >> "${CURRENTFILE}"
    pin(${PINMATCHES[$i]}) {
        direction : `echo "${PINDIR};" | tr '[:upper:]' '[:lower:]'`
        ${LOADSTR}
        related_power_pin : ${SUPARR[0]};
        related_ground_pin : ${GNDARR[0]};
    }
EOF
fi
fi
done

echo "Found bus names:"
for i in "${!VLOGBUSNAMES[@]}"; do

BNAME=${VLOGBUSNAMES[$i]}
cat << EOF >> "${CURRENTFILE}"
    bus(${BNAME}) {
        bus_type : ${BUSTYPES[$i]};
EOF
# Extract indexes in braces
IDCS=$(sed -n "s/\(PIN \)\(${BNAME}\)\(\[.*\]\).*/\3/p" ${INFILE})

IDARR=(${IDCS})
echo "> ${BNAME} (${#IDARR[@]} bits)"
for idx in $IDCS
do
PINNAME=${BNAME}${idx}
for i in "${!PINMATCHES[@]}"; do
    if [[ "${PINMATCHES[$i]}" = "${PINNAME}" ]]; then
        PINDIR=${DIRMATCHES[$i]}
        if [ ${PINDIR} == "INPUT" ]; then
            LOADSTR="capacitance : ${INPUTCAP};"
        else
            LOADSTR="max_capacitance : ${MAXCAP};"
        fi
    fi
done
cat << EOF >> "${CURRENTFILE}"
        pin(${PINNAME}) {
            direction : `echo "${PINDIR};" | tr '[:upper:]' '[:lower:]'`
            ${LOADSTR}
            related_power_pin : ${SUPARR[0]};
            related_ground_pin : ${GNDARR[0]};
        }
EOF
done
cat << EOF >> "${CURRENTFILE}"
    }
EOF
done
cat << EOF >> "${CURRENTFILE}"
}
/*Cell definition end*****************************/
}
EOF

for op in ${OPCONDS}
do
CURRENTFILE="${TARGETDIR}/${CELLNAME}_${op}.lib"
cat ${TARGETDIR}/${CELLNAME}_celldef.lib >> ${CURRENTFILE}
done
rm ${TARGETDIR}/${CELLNAME}_celldef.lib
echo "LIBs generated!"

exit 0

