#!/usr/bin/env bash
#############################################################################
# This is a design imporrt script from digital flow to Cadence Virtuoso
# design environment.
# The purpose is to generate all needed files
# in parametrized manner from this single file
# Created by Marko Kosunen on 24.03.2015
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f() {
cat << EOF
DIGI2VIRTUOS0 Release 1.1 (14.04.2024)
digi2virtuoso-import verilog and GDS files to Cadence Virtuoso defaulting to 
be compatible with Innovus digital design flow.
Written by Marko Pikkis Kosunen

SYNOPSIS
  digi2virtuoso [OPTIONS]
DESCRIPTION
  Imports verilog and GDS files to Cadence Virtuoso so that the reulting 
  views can be used for handling digital designs in Virtuoso environment. 

OPTIONS
  -c
     Top cell name. If not given, imports all the modules in the files (i.e. library import)
  -f
     Force library owerwriting
  -g
      GDS file name.
  -i
      Import modules file [STRING]
  -I
      Ignore modules file [STRING]
  -l
      Virtuoso library name.
  -o
     Overwrite existing views
  -O
     DO NOT overwrite symbols. Use together with -o if you want to append to
     existing view
  -p
      Primitive. Copies symbol view to auCdl view.
      auCdl view indicates primitive for the netlister.
  -r
      Reference library list "STRING". Space separated list of reference
      libraries.
  -R
      Reference Schematic view list "STRING". Space separated list of views
      that are used as reference for pins (i.e. supply, ground) tha are
      present in the Verilog, but not the symbol. Not needed if the pins in
      verilog match the symbol.
  -s
      Font scaling factor.
  -t
      Technology library.
  -v
      VERILOG netlist.
  -h
      Show this help.
EOF
}

CELL=""
VIRTUOSOTARGETLIB=""
ISPRIM="0"
REFLIBS=""
REF_SCH_VIEWS=""
NETLIST=""
IMPORTMODFILE=""
IGNOREMODFILE=""
FORCE="0"
OVERWRITE="0"
TECHLIB=""
FONTSCALE="1"
OVERWRITESYMBOL="1"
while getopts c:fg:i:I:l:oOpr:R:s:t:v:h opt
do
  case "$opt" in
    c) CELL=${OPTARG};;
    f) FORCE="1";;
    g) GDS=${OPTARG};;
    i) IMPORTMODFILE=${OPTARG};;
    I) IGNOREMODFILE=${OPTARG};;
    l) VIRTUOSOTARGETLIB=${OPTARG};;
    o) OVERWRITE="1";;
    O) OVERWRITESYMBOL="0";;
    p) ISPRIM="1";;
    r) REFLIBS=${OPTARG};;
    R) REF_SCH_VIEWS=${OPTARG};;
    s) FONTSCALE=${OPTARG};;
    t) TECHLIB=${OPTARG};;
    v) NETLIST=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done
if [ -z "$TECHLIB" ]; then
    echo "Technology library not given"
    help_f;
    exit 0
fi

echo $REFLIBS | sed 's/ /\n/g' > /tmp/$$_reflib.list
HDLREFLIBS="`echo ${REFLIBS} | sed 's/ /,/g'`"

VERILOGREFVIEWS="`echo ${REF_SCH_VIEWS} | sed 's/ /,/g'`"

if [ ! -f "${GDS}" ]; then
    echo "Layout file does not exist."
    echo "importing only verilog"
fi

if [ ! -d ${VIRTUOSOTARGETLIB} ]; then
virtuoso -nograph << EOC
    dbCreateLib("${VIRTUOSOTARGETLIB}" "./")
    techBindTechFile(ddGetObj("${VIRTUOSOTARGETLIB}") "${TECHLIB}" "tech.db" t)
    exit
EOC

#Force removal in different 'virtuoso session' to avoid need for refreshing
elif [ "${FORCE}" == "1" ]; then
    if [ -d ${VIRTUOSOTARGETLIB} ]; then
        echo "Forcing library deletion"
virtuoso -nograph << EOC
    ddDeleteObj(ddGetObj("${VIRTUOSOTARGETLIB}"))
    exit
EOC

# Check if removalwas succesfull
    SUCCESS="0"
    COUNT=5
        while [ $SUCCESS == "0" -a $COUNT -le 30 ]; do
            if [ ! -d ${VIRTUOSOTARGETLIB} ]; then
                SUCCESS="1"
            else
                sleep 5
                COUNT=$((COUNT+5))
            fi
        done
        if [ $SUCCESS == "0" ]; then
            echo "ERROR forced removal of library ${VIRTUOSOTARGETLIB} failed"
            exit 1
        else
            echo "Forced removal of library ${VIRTUOSOTARGETLIB} succesfull"
        fi
    fi

echo "Create library ${VIRTUOSOTARGETLIB}"
virtuoso -nograph << EOC
    dbCreateLib("${VIRTUOSOTARGETLIB}" "./")
    techBindTechFile(ddGetObj("${VIRTUOSOTARGETLIB}") "${TECHLIB}" "tech.db" t)
    exit
EOC

elif  [ -d "${VIRTUOSOTARGETLIB}" -a "${OVERWRITE}" == "0" ]; then
    echo "Library ${VIRTUOSOTARGETLIB} exists. Existing cells will not be overwritten."
    echo "Force delete with option -f or or force overwrite with -o"
    echo "Layouts will be overwritten"
fi

###########################################################################################
## Import the design
###########################################################################################
#Stream in the GDS
if [ -f "${GDS}" ]; then

    if [ -z "${CELL}" ]; then
        echo " No top cell given, importing everything from the GDS."
        TOPCELLSTRING=""
    else
        TOPCELLSTRING="-topCell ${CELL}"
    fi
    strmin -library $VIRTUOSOTARGETLIB -strmFile ${GDS} ${TOPCELLSTRING} \
        -writeMode overwrite \
        -scaleTextHeight ${FONTSCALE} \
        -refLibList "/tmp/$$_reflib.list" \
        -logFile ./strmIn.log \
        -numThreads 12 \
        -replaceBusBitChar \
        -wildCardInCellMap
    rm -f /tmp/$$_reflib.list
fi

if [ ! -z "$NETLIST" ]; then
    if [ ! -f "${NETLIST}" ]; then
        echo "Netlist file does not exist."
        help_f;
        exit 1;
    else
        #Import Verilog
        echo "Importing verilog"
        echo "Tempdir is $TMPDIR"
cat > /tmp/$$_ihdl_files   << EOF
dest_sch_lib := ${VIRTUOSOTARGETLIB}
ref_lib_list := ${HDLREFLIBS}
import_if_exists := ${OVERWRITE}
overwrite_symbol := ${OVERWRITESYMBOL}
$( if [ -d "$TMPDIR" ]; then
echo "work_area := ${TMPDIR}"
fi )
$( if [ -f "$IMPORTMODFILE" ]; then
echo "import_mod_file := ${IMPORTMODFILE}"
fi )
$( if [ -f "$IGNOREMODFILE" ]; then
echo "ignore_node_file := ${IGNOREMODFILE}"
fi )
schematic_view_name := schematic
pnr_max_inst := 10
pnr_max_port := 10
netlist_view_name := netlist
symbol_view_name := symbol
functional_view_name := functional
log_file_name := ./verilogIn.log
structural_views := 1
$( if [ ! -z "$REF_SCH_VIEWS" ]; then
    echo "ref_sch_list := ${VERILOGREFVIEWS}"
fi )
power_net := THIS_IS_A_FAKE_NAME_FOR_POWER_NET_$$
ground_net := THIS_IS_A_FAKE_NAME_FOR_GND_NET_$$
EOF

        ihdl -param /tmp/$$_ihdl_files -cdslib ./cds.lib ${NETLIST}
        rm -f /tmp/$$_ihdl_files
    fi
fi

#Copy symbol to auCdl view to make a primitive for netlisting
if [ "${ISPRIM}" == "1" ]; then
    if [ -z "${CELL}" ]; then
        echo "No top cell given. Creating auCdl views from symbol views for all cells in library ${VIRTUOSOTARGETLIB}"
virtuoso -nograph  << EOC
srclib=ddGetObj("${VIRTUOSOTARGETLIB}")
foreach(cell srclib~>cells
    printf("Copying symbol to auCdl for %s\n" cell~>name)
    cv=dbOpenCellViewByType("${VIRTUOSOTARGETLIB}" cell~>name "symbol")
    cvc=dbCopyCellView(cv "${VIRTUOSOTARGETLIB}" cell~>name "auCdl" nil nil t)
    dbClose(cv)
    dbClose(cvc)
)
exit
EOC

    else
        echo "Creating auCdl view for primitive ${CELL} in library ${VIRTUOSOTARGETLIB}"
        virtuoso -nograph  << EOC
cv=dbOpenCellViewByType("${VIRTUOSOTARGETLIB}" "${CELL}" "symbol")
cvc=dbCopyCellView(cv "${VIRTUOSOTARGETLIB}" "${CELL}" "auCdl" nil nil t)
dbClose(cv)
dbClose(cvc)
exit
EOC

    fi
fi

#Remap layers
#virtuoso -nograph  << EOC
#;Remap the pin layers. This should be defined already in Encounter
#;cv=dbOpenCellViewByType( "$VIRTUOSOTARGETLIB"  "$CELL" "layout" "" "a")
#cv=ddsServOpen("$VIRTUOSOTARGETLIB" "$CELL" "layout" "edit" nil)
#layerList = '( "list of layers here" "L2" )
#foreach( currentLayer layerList
#    leSetAllLayerSelectable( nil )
#    leSetAllObjectsSelectable( t )
#    leSetLayerSelectable( list(currentLayer "pin") t )
#    geSelectAllFig()
#    LP=geGetSelSet()
#    LP~>purpose="drawing"
#    geDeselectAll()
#)
#leHiSave()
#exit
#EOC

exit 0

