#!/bin/sh
#############################################################################
# This is a script for making a "release" in Chip'nDale SoC flow 
#
# It automates the git related steps for you.
#
# Created by Marko Kosunen on 30.9.2022 
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
SCRIPTNAME="macro_release.sh"
cat << EOF
${SCRIPTNAME} Release 1.0 (30.09.2022)
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])-Macro release in Chip'nDale flow"
Written by Marko "Pikkis" Kosunen"

     SYNOPSIS
       $SCRIPTNAME [OPTIONS] 
     DESCRIPTION
       Perform a macro release inside a Chip'nDale project. 
       Assumes standard Chip'nDale project stucture with
       macro_repository submodule containing the submodule for the macro.

       Defaults to current branch of master project and 'master' branches of submodules.

     OPTIONS
       -c
           Top cell name. STRING
       -d
           Extract digital macro files (LEF, LIB, Verilog). Not extracted by default.
       -D
           Export DEF file. Not extracted by default.
       -f
           Force library owerwriting. 
       -G
           Ground net name list "STRING". Default "VSS". 
       -l
           Virtuoso target library name (to be created in)  current directory. STRING
       -m 
           Commit message for the macro. Used for the filesystem macro repository.
       -M
           Branch of the macro in macro_repository/<cell> submodule  "STRING". Default 'master'
       -r
           Library exclusion list "vG".
       -O
          Overwrite the requirement of having VSS and VDD as the first entries of supply 
          and ground net lists.
       -R
           Branch of the macro_repository submodule  "STRING". Default 'master'
       -s
           Virtuoso source library name. STRING
       -S
           Supply net name list "STRING". Default "VDD". 
       -t
           Technology library.
       -V
          Supply net voltage list. "v1 v2..." To override the default voltage for all supply nets, 
          use -V "0.88" . Otherwise the the length of the list must match the length of the 
          supply net list.
       -Y  
           Answer 'y' to all confirmations.
       -h
           Show this help.
EOF
}


# Print and eval a command string
inform_eval()
{
    #remove linebreaks and double spaces
    CMD="$(echo "$1" | tr -d \\n | sed 's/\s\s*/ /g')"
    echo -e " $CMD \n"
    eval $CMD || $(echo "$CMD failed." && return 1) 
}


# Print and eval a command string conditionally
inform_eval_test()
{
 # $1 is the command
 # $2 is the tested variable
 # $3 is the condition
if [ "$2" == "$3" ]; then
    inform_eval "$1" || $(echo "function inform_eval failed." && return 1) 
else
    echo "Exit by user input"
    exit 0
fi
}

#Rever atll changes done in with git 
revert_changes() 
{ 
    echo "Reverting all changes"
    echo "Resetting $CELLREPODIR to $CELLREPOCOMMIT"
    inform_eval "cd $CELLREPODIR && git reset --hard $CELLREPOCOMMIT"
    echo "Resetting $MACROREPODIR to $MACROREPOCOMMIT"
    inform_eval "cd $MACROREPODIR && git reset --hard $MACROREPOCOMMIT"
    echo "Resetting master project to $MASTERREPOCOMMIT of $MASTERCURRBRANCH"
    inform_eval "cd ${PROJDIR} && git checkout $MASTERCURRBRANCH && git reset --soft $MASTERREPOCOMMIT && cd macro_repository && ./init_submodules.sh"

}

# Ask confirmation for actions
confirm() 
{
    ANS="0"
    if [ "$YES" == "0" ]; then
        echo "Is this what you want? [y|n]"
        read ANS
    else
        ANS="y"
    fi
}

# Check function for final pushes
check_push()
{
if [ "$ANS" == "y"  ]; then
    PUSH="1"
else
    revert_changes
    echo "Exit by user input"
    exit 0
fi
}

# Directory variables
THISDIR="$(pwd)"
PROJDIR="$(cd ${THISDIR}/.. && pwd )"
#Global variables for functions
ANS=""
PUSH="0"
# Variables for arguments
CELLNAME=
VIRTUOSOTARGETLIB=
REFLIBS=
NETLIST=
FORCE=""
DIGIEXT=""
DEFOUT=""
GNDNETS="VSS"
SUPNETS="VDD"
MACROCOMMITMSG=""
MACROREPOBRANCH="master"
CELLREPOBRANCH="master"
YES="0"
VOLTAGES="0.9"
SUPNETSOVERRIDE=""

while getopts c:dDfG:l:m:M:Or:R:S:s:t:V:Yh opt
do
  case $opt in
    c) CELLNAME="${OPTARG}";CELL="-c ${OPTARG}";; 
    d) DIGIEXT="-d";;
    D) DEFOUT="-D";;
    f) FORCE="-f";;
    G) GNDNETS=${OPTARG};;
    l) VIRTUOSOTARGETLIB="-l ${OPTARG}";;
    m) MACROCOMMITMSG=${OPTARG};;
    M) MACROREPOBRANCH=${OPTARG};;
    O) SUPNETSOVERRIDE=" -O ";;
    r) REFLIBS="-r \"$(echo ${OPTARG} | tr -d \\n)\"";;
    R) CELLREPOBRANCH=${OPTARG};;
    s) VIRTUOSOSOURCELIB="-s ${OPTARG}";;
    S) SUPNETS=${OPTARG};;
    t) TECHLIB="-t ${OPTARG}";;
    V) VOLTAGES=${OPTARG};;
    Y) YES="1";; 
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

# Test the structure
if [ ! -f "${THISDIR}/shell/release_cell.sh" ]; then
    echo "You are not in Chip'nDale Virtuoso directory"
    echo "You must be in Chip'nDale Virtuoso directory"
    exit 1
fi
if [ ! -d "${PROJDIR}/macro_repository" ]; then
    echo "Directory ${PROJDIR}/macro_repository does not exist."
    echo "Not Chip'nDale project structure."
    exit 1
fi
if [ ! -f "${PROJDIR}/macro_repository/${CELLNAME}/${CELLNAME}.tbz2" ]; then
    echo "${PROJDIR}/macro_repository/$CELLNAME/${CELLNAME}.tbz2 does not exist" 
    echo "No current macro release to update."
    exit 1
fi

SUPARR=(${SUPNETS})
GNDARR=(${GNDNETS})
if [ -z "${SUPNETSOVERRIDE}" ]; then
    if [ "${SUPARR[0]}" != "VDD" ]; then
        cat << EOF  
"Primary supply net is not VDD but ${SUPARR[0]}.  This will most likely break the digital 
flow power domain configuration. Override with option -O if you are confident 
about what you are doing.
EOF
        exit 1
    fi
    if [ "${GNDARR[0]}" != "VSS" ]; then
        cat << EOF  
"Primary ground net is not VSS but ${GNDARR[0]}.  This will most likely break the digital 
flow power domain configuration. Override with option -O if you are confident 
about what you are doing.
EOF
        exit 1
    fi
fi

#Modify these to arguments
SUPNETS="-S \"$(echo ${SUPNETS} | tr -d \\n)\""
GNDNETS="-G \"$(echo ${GNDNETS} | tr -d \\n)\""

# Execute release_cell.sh
CMD="${THISDIR}/shell/release_cell.sh \
    $CELL \
    $DIGIEXT \
    $DEFOUT \
    $FORCE \
    $GNDNETS  \
    $VIRTUOSOTARGETLIB  \
    $REFLIBS  \
    $VIRTUOSOSOURCELIB  \
    $SUPNETS $SUPNETSOVERRIDE $TECHLIB\
"
if [ ! -f "${THISDIR}/${CELLNAME}.tbz2" ]; then
    inform_eval "$CMD" || $(echo "Cell release failed" && exit 1)
else
    echo "Release file ${CELLNAME}.tbz2 exists. Will make a new release." 
    confirm
    inform_eval_test "$CMD" "$ANS" "y" || $(echo "Cell release failed" && exit 1)

fi



#Prepare the master project
cd $PROJDIR
MASTERREPOCOMMIT="$(git rev-parse --verify HEAD)" 
MASTERCURRBRANCH="$(git branch --show-current)"
echo -e "Pull in the latest commit of ${MASTERCURRBRANCH} branch of the master project at"
echo "${PROJDIR},"
echo -e "and initialize macro_repository submodule\n"

confirm
CMD="cd $PROJDIR && git pull && git submodule update --init --recursive macro_repository"
inform_eval_test "$CMD" "$ANS" "y"

# Prepare macro_repository
MACROREPODIR="${PROJDIR}/macro_repository" 
cd ${MACROREPODIR} && echo -e "Changing to ${MACROREPODIR}\n"

MACROREPOCOMMIT="$(git rev-parse --verify HEAD)" 
MACROCURRBRANCH="$(git branch --show-current)"
if [ -z "$CURRBRANCH" ]; then
    echo "Submodule macro_repository in DETACHED HEAD state at commit $MACROREPOCOMMIT"
    echo -e "Check out $MACROREPOBRANCH branch and pull in the latest commit\n"
else  
    echo -e "Submodule macro_repository in $MACROCURRBRANCH, at commit $MACROREPOCOMMIT \n"
    echo "Check out $MACROREPOBRANCH ,pull in the latest commit,"
    echo -e "and initialize the $CELLNAME submodule\n"
fi

confirm
CMD="git checkout $MACROREPOBRANCH && git pull && git submodule update --init ./${CELLNAME}"
inform_eval_test "$CMD" "$ANS" "y"

# Preparing cell submodule
CELLREPODIR="${MACROREPODIR}/${CELLNAME}" 
cd ${CELLREPODIR} && echo -e "Changing to ${CELLREPODIR}\n"

CELLREPOCOMMIT="$(git rev-parse --verify HEAD)" 
CELLCURRBRANCH="$(git branch --show-current)"
if [ -z "$CELLCURRBRANCH" ]; then
    echo "$CELLNAME repository in DETACHED HEAD state at commit $CELLREPOCOMMIT"
    echo -e "Check out $CELLREPOBRANCH and pull in the latest commit.\n"
else  
    echo "Macro_repository in $CELLCURRBRANCH, at commit $CELLREPOCOMMIT"
    echo -e "Check out $CELLREPOBRANCH, and pull in the latest commit.\n"
fi

confirm
CMD="git checkout $CELLREPOBRANCH && git pull"
inform_eval_test "$CMD" "$ANS" "y"

if [ -f "${THISDIR}/${CELLNAME}.tbz2" ]; then
    echo "Move ${THISDIR}/${CELLNAME}.tbz2" 
    echo "to"
    echo "${CELLREPODIR}/${CELLNAME}.tbz2 ,"
    echo -e "Stage changes and commit\n" 
    confirm
    if [ "$ANS" == "y"  ]; then
        inform_eval "mv ${THISDIR}/${CELLNAME}.tbz2 ${CELLREPODIR}"
        if [ -z "$MACROCOMMITMSG" ]; then
            inform_eval "cd ${CELLREPODIR} && git add ${CELLNAME}.tbz2 && git commit"
        else
            inform_eval "cd ${CELLREPODIR} && git add ${CELLNAME}.tbz2 && git commit -m\"${MACROCOMMITMSG}\""
        fi
    else
        revert_changes
        echo "Exit by user input"
        exit 0
    fi
else 
    echo "Release ${CELLNAME}.tbz2 does not exist. Release failed"
    revert_changes
    exit 1
fi

#Macro 
echo -e "Stage ${MACROREPODIR}/${CELLNAME} submodule and commit.\n" 
confirm
CMD="cd ${MACROREPODIR} && git add ${CELLNAME} && git commit -m\"Release ${CELLNAME}\""
inform_eval_test "$CMD" "$ANS" "y"

#Macro repository
echo -e "Stage ${MACROREPODIR} submodule and commit.\n" 
confirm
CMD="cd ${PROJDIR} && git add ${MACROREPODIR} && git commit -m\"Release ${CELLNAME}\""
inform_eval_test "$CMD" "$ANS" "y"

# Pushing
PUSH="0"
echo "Pushing submodules...."
for string in "OK to push $CELLREPODIR/${CELLNAME}.tbz2 ?" \
    "OK to push $MACROREPODIR/${CELLNAME} ?" \
"OK to push $MACROREPODIR at master project  ?"; do
    echo $string
    confirm
    check_push
done

if [ "$PUSH" == "1" ]; then
    for dir in "$MACROREPODIR/${CELLNAME}"  "$MACROREPODIR" "$PROJDIR"; do
        inform_eval "cd $dir && git push"
    done
else
    # We should not end up here
    echo "Error occurred, reverting changes"
    revert_changes
    exit 1
fi

echo "Release of cell $CELLNAME succesfull."
exit 0

