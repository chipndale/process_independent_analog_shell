#!/bin/sh
#############################################################################
# This is a script for updating references in a Cadence virtuoso library.
#
# Created by Marko Kosunen on 29.10.2018 
# Last modification by Marko Kosunen, marko.kosunen@aalto.fi, 29.10.2018 02:33
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
cat << EOF
update refs Release 1.0 (28.10.2018)"
updates the library references in given virtuoso library
Written by Marko "Pikkis" Kosunen

 SYNOPSIS
   release_cell [OPTIONS] 
 DESCRIPTION
   Compress a virtuoso cell views to a single bzip2 file

 OPTIONS
   -f
      From library 
   -l
       Virtuoso library name to operate in. STRING
   -t
       To libary.
   -h
       Show this help.
EOF
}

while getopts f:l:t:h opt
do
  case "$opt" in
    f) FROMLIB=${OPTARG};;
    l) VIRTUOSOLIB=${OPTARG};;
    t) TOLIB=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done
# Only the views schematic, layout and symbol are released
virtuoso -nograph << EOC
fromlib=gdmCreateSpec( "${FROMLIB}" "" "" "" "CDBA" )
tolib=gdmCreateSpec( "${TOLIB}" "" "" "" "CDBA" )
vlib=gdmCreateSpec( "${VIRTUOSOLIB}" "" "" "" "CDBA" )
vliblist=gdmCreateSpecList()
gdmAddSpecToSpecList(vlib vliblist)
ccpRenameReferenceLib( fromlib tolib vliblist 'CCP_VALID_TO_REFS ) 
exit

EOC

unset CDS_COPY_AUTO_RENAME
exit 0

