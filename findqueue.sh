#!/bin/sh
#############################################################################
# This is a script to launch a LSF job to a que that has most availabe slots 
# in a given queue list
# Created by Marko Kosunen on 16.10.2018
# Last modification by Marko Kosunen, marko.kosunen@aalto.fi, 16.10.2018 20:08
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
SCRIPTNAME="finqueues"
cat << EOF
${SCRIPTNAME} 1.0 (16.10.2018)
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])-run the job in the best quue
Written by Marko "Pikkis" Kosunen"

SYNOPSIS
$(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:])  [OPTIONS]
DESCRIPTION
    Finds the shortest LSF queue

OPTIONS
  -n
      Slots needed   
  -q  
      "string" Queue list
  -h
      Show this help.
EOF
}
SLOTS="1"
while getopts n:q:h opt
do
  case "$opt" in
    n) SLOTS=${OPTARG};; 
    q) QUEUES=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done
if [ -z "$QUEUES" ]; then
    echo "ERRROR:At least one queue should be given"
    help_f;
    exit 1;
fi

BEST=""
DIFFBEST="-1"
while [ $DIFFBEST -lt 0 ]; do 
    for queue in $QUEUES;do 
        HOSTLIST=$(bqueues -l $queue | sed -ne '/^HOSTS/p' | sed -n 's/\(^HOSTS:  *\)\(.*$\)/\2/p')
        MAX="0"

        for host in $HOSTLIST; do
            HMAX=$(bhosts $host | awk 'FNR==2{print $4}')
            MAX=$((MAX + HMAX))
        done
        QUEUED=$(bqueues $queue | awk 'FNR==2{print $8}')
        #echo Maximum number of jobs in $queue is $MAX , queued $QUEUED
        DIFF=$((MAX - QUEUED-$SLOTS))
        if [ $DIFF -ge $DIFFBEST ]; then
            BEST="$queue"
            DIFFBEST="$DIFF"
        fi
    done
done
#echo "The best queue is $BEST"
echo "$BEST"
exit 0

