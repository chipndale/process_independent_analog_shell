#!/usr/bin/env bash
#############################################################################
# A wrapper for process dependent memmory generator 
# Aim is to provide a unified input for various process specific generators
# So that the memory parameters provided by Chisel memorymapping .conf file are
# Ampped to vendor specific paramaters.
# After generation, the memory is wrapped in a format the can be released
# with chip-and-dale flow macro release procedure.
# This script is closely connected to memory_mapper.sh script present in Innovus flow.
#
# Created by Marko Kosunen on 25.10.2021
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
SCRIPTNAME="memory_generator.sh"
cat << EOF
${SCRIPTNAME} Release 1.0 (25.10.2021)
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])-Chisel and chip-and-dale wrapper for 
process specific memory generator"

Written by Marko "Pikkis" Kosunen"

SYNOPSIS
$(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:])  [OPTIONS]
DESCRIPTION
    Generates a memory block from given .conf file with process specific naming conventions.

    Config file syntax follows the Chisel memory mapping config file. Each line defines a memory
    Line syntax is:
    "name mem_ext depth 16384 width 32 ports write,read"
    Define depth and width according to your needs.

OPTIONS
  -c
      Config file   

  -h
      Show this help.
EOF
}

WORKDIR=$(pwd)

while getopts c:w:h opt
do
  case "$opt" in
    c) CONFFILE=${OPTARG};; 
    w) WORKDIR=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

if [ -z ${CONFFILE} ]; then
    echo "Memory mapper config file not given"
    help_f;
    exit 1
fi
NAMES=($(awk '{print $2}' ${CONFFILE} | xargs))
DEPTHS=($(awk '{print $4}' ${CONFFILE} | xargs))
WIDTHS=($(awk '{print $6}' ${CONFFILE} | xargs))
TYPES=($(awk '{print $8}' ${CONFFILE} | xargs | sed 's/,//g'))

. ${PROJECT_FOLDER}/environment_setup/memory_generator_files/memory_generator_include.sh


exit 0


