#!/bin/sh
#############################################################################
# This is a design import script from digital flow to Cadence Virtuoso 
# design environment.
# The purpose is to generate all needed files
# in parametrized manner from this single file
# Created by Marko Kosunen on 24.03.2015
# Last modification by Marko Kosunen, marko.kosunen@aalto.fi, 12.10.2018 19:13
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
SCRIPTNAME="subckt_sortpins"
cat << EOF
${SCRIPTNAME} Release 1.0 (12.09.2018)
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])-Sorts the pins of a Spice subcircuit
Written by Marko "Pikkis" Kosunen"

SYNOPSIS
$(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:])  [OPTIONS]
DESCRIPTION
    Sorts the pins of a subcircuit in spice file to correspond the default output 
    order of Cadnce si netlister. Default is Alphabetical names, little endian buses, bus edlimiter <>
OPTIONS"
  -d 
      Destination path
  -s
      Source path
  -h
      Show this help.
EOF
}
SOURCEFILE=""
DESTFILE=""
while getopts d:s:S:h opt
do
  case "$opt" in
    s) SOURCEFILE=${OPTARG};;
    d) DESTFILE=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done
if [ -z ${SOURCEFILE} ]; then
    echo -e "ERROR: Source file not given\n"
    help_f;
    exit 1
fi
if [ -z ${DESTFILE} ]; then
    echo -e "ERROR:Destination file not given\n"
    help_f;
    exit 1
fi

echo -n "" > $DESTFILE

#Disable wildcards
set -f

#read line by line
while IFS= read -r line || [ -n "$line" ]; do
    split=( $line )
    if [ ${split[0]} == ".subckt" ]; then
        module=${split[@]:0:2}
        pins=${split[@]:2}
        pins_sorted=$(echo $pins | xargs -n1 | sort | xargs)
        echo "$module $pins_sorted" >> $DESTFILE
    else    
        echo "$line" >> $DESTFILE
    fi
done <$SOURCEFILE 

#Bye bye
exit 0

