# Proces independent shell scripts for Chip'n Dale 

This is the colelctfor the Dale flow ion of process independent shell schipt
used by the flow top level. Digital scripts for the Dale flow are stored at
(https://gitlab.com/chipndale/process_independent_digital_shell)[https://gitlab.com/chipndale/process_independent_digital_shell]

# Contributors at the release date 7.1.2024

Individuals below have contributed to the development of thesde scripts by the
release date 7.1.2024. This to acknowledge them as we had to purge the commit
history to preven untintentional leaks of confidential information.

* Marko Kosunen 
* Okko Järvinen
* Santeri Porrasmaa
* Aleksi Korsman
* Ilia Kempi
top 
