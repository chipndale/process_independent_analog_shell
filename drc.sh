#!/bin/sh
#############################################################################
# This script runs DRC, antenna and bump decks for a gds file
#
# Initially written by Kosta Trotskovsky, 08/23/2018
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
cat << EOF
DESCRIPTION
    Runs DRC, antenna and bump decks for a gds file

OPTIONS"
  -a
      Runs antenna (no further arguments)
  -b
      Runs bump (no further arguments)
  -c
      Top cell name.
  -d
      Runs DRC (no further arguments)
  -D
      Directive [string]. Custom string to be passed to
      process and tool spesific check defintions for further i
      control. Default \"\"
  -f
      Force target directory overwrite (no further arguments)
  -g
      GDS file name.
  -l
      Target directory for the results
  -L
      Disable LSF execution.
  -h
      Show this help.
EOF
}


RUNDRC="0"
RUNANTENNA="0"
RUNBUMP="0"
FORCE="0"
CELL=""
WORKDIR=""
GDS=""
DIRECTIVE=""
QUEUE=""
LSFSUB="bsub -I -n 16 -R \"span[hosts=1]\""
while getopts abc:dD:fg:l:Lq:h opt
do
  case "$opt" in
    a) RUNANTENNA="1";;
    b) RUNBUMP="1";;
    c) CELL=${OPTARG};;
    d) RUNDRC="1";;
    D) DIRECTIVE=${OPTARG};;
    f) FORCE="1";;
    g) GDS=${OPTARG};;
    l) WORKDIR=${OPTARG};;
    L) LSFSUB="";;
    q) QUEUE=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

if [ ! -z "${QUEUE}" ]; then
    echo "WARNING Option -q is obsolete"
fi

if [ "${RUNDRC}" == '0' ] && [ "${RUNANTENNA}" == '0' ] && [ "${RUNBUMP}" == '0' ] ; then
    echo "At least one of DRC, antenna or bump should be speficified!"
    exit 1
fi

if [ -z "$CELL" ]; then
    echo "Top cell name not given"
    help_f;
    exit 1

fi

if [ -z "$WORKDIR" ]; then
    echo "Target directory not given"
    help_f;
    exit 1
fi

if [ -z "$GDS" ]; then
    #This is the default GDS  extracted from the layout
    #Otherwise use give file
    echo "GDS file does not given"
    help_f;
    exit 1
elif [ ! -f "$GDS" ]; then
    echo "GDS file $GDS does not exist"
    exit 1
fi

if [ -d $WORKDIR ]; then
    if [ "${FORCE}" == '1' ]; then
        rm -rf "${WORKDIR}"
        mkdir -p "${WORKDIR}"
    else
        echo "Target directory exists. Use a different name or -f to overwrite."
        exit 1
    fi
else
    mkdir -p "${WORKDIR}"
fi

#Result directories
WORKDIR="$(readlink -f $WORKDIR)"
DRCDIR="${WORKDIR}/drc"
ANTENNADIR="${WORKDIR}/antenna"
BUMPDIR="${WORKDIR}/bump"

#This defines how we run the checks
#Utilize DIRECTIVE to define customzed behaviour
. ${DRC_FILES_DIR}/drc_include.sh

#Wait the background processes to finish
sleep 1
echo "Running checks in parallel in background."
echo "See logfiles in ${WORKDIR} for progress"
echo -n "Running DRC with PID's ${PIDLIST}..."
while [ ! -z "$PIDLIST" ]; do
    PIDLIST=$(ps -p ${PIDLIST} | awk '{if (NR!=1) {print $1}}')
    sleep 5
done
echo "done!"
exit 0

