#!/bin/sh
#############################################################################
# This script runs dummyfill for a gds file
# Originally written by Kosta Trotskovsky, 08/23/2018
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
cat << EOF
DESCRIPTION
    Runs dummyfill on a gds file

OPTIONS"
  -f
      Force target directory overwrite (no further arguments)
  -c
      Top cell name.   
  -D
      Directive [string]. Custom string to be passed to
      process and tool spesific check defintions for further
      control. Default \"\"
  -l
      Target directory for the dummyfilled GDS.
  -g
      GDS file name.
  -h
      Show this help.
EOF
}

FORCE="0"
CELL=""
TARGETLIB=""
GDS=""
DIRECTIVE=""
while getopts fc:D:l:g:d:h opt
do
  case "$opt" in
    f) FORCE="1";;
    c) CELL=${OPTARG};; 
    D) DIRECTIVE=${OPTARG};; 
    l) TARGETLIB=${OPTARG};;
    g) GDS=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

if [ -z "$CELL" ]; then
    echo "Top cell name not given"
    help_f;
    exit 1
fi

if [ -z "$TARGETLIB" ]; then
    echo "Target directory for the dummyfilled GDS not given"
    help_f;
    exit 1
fi

if [ -z "$GDS" ]; then
    #This is the default GDS  extracted from the layout
    #Otherwise use give file
    GENGDS="1"
    GDS=${CELL}.calibre.db
elif [ ! -f "$GDS" ]; then
    echo "GDS file $GDS does not exist"
    exit 1
fi


# Create a target library
if [ -d $TARGETLIB ]; then
    if [ "${FORCE}" == '1' ]; then
        rm -rf $TARGETLIB
    else
        echo "Target directory exists. Use a different name or -f to overwrite."
        exit 1
    fi
fi
mkdir -p $TARGETLIB
. ${PROJECT_FOLDER}/environment_setup/dummyfill_files/dummyfill_include.sh

exit 0

