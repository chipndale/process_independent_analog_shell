#!/bin/sh
#############################################################################
# This script generates a LEF file from an analog circuit layout
# to be used by a digital place-and-route tool (Encounter/Innovus).
# Created by Okko Järvinen on 25.10.2019
# Last modification by Kalle Spoof, kalle.spoof@aalto.fi, 24.11.2020 10:42
#############################################################################
#Function to display help with -h argument and to control
#the configuration from the command line
help_f()
{
SCRIPTNAME="generate_lef"
cat << EOF
${SCRIPTNAME} Release 1.0 (24.10.2019)
Generates LEF from Virtuoso layout.
Written by Okko Järvinen.

SYNOPSIS
    $(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:]) [OPTIONS]
DESCRIPTION
    Generates abstract view and LEF file from Virtuoso layout.

OPTIONS
  -c
      Top cell name.   
  -d  
      Target directory. Results will be stored here. Default <working dir>/<Top cell name>_lef  
  -D
      Directive [string]. Custom string to be passed to
      process and tool spesific check defintions for further i
      control. Default \"\"
  -f
      Force result overwriting.
  -G 
      Ground net name list "STRING". Default "VSS".
  -l
      Virtuoso library name.
  -S  
      Supply net list "STRING". Default "VDD".
  -w 
      Working directory. Must contain cds.lib file. Default: current directory.
  -h
      Show this help.
EOF
}
WORKDIR=`pwd`
CELLNAME=""
LIBNAME=""
FORCE="0"
GNDNETS="VSS"
SUPNETS="VDD"
DIRECTIVE=""

while getopts c:d:fG:l:S:w:h opt
do
  case "$opt" in
    c) CELLNAME=${OPTARG};; 
    d) TARGETDIR=${OPTARG};;
    f) FORCE="1";;    
    G) GNDNETS=${OPTARG};;
    l) LIBNAME=${OPTARG};;
    S) SUPNETS=${OPTARG};;
    w) WORKDIR=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

# Create the lef directory
if [ -z "$TARGETDIR" ]; then
    TARGETDIR="${WORKDIR}/${CELLNAME}_lef"
fi
if [ -d "$TARGETDIR" ]; then
    if [ "$FORCE" == "1" ]; then
        rm -f "$TARGETDIR/${CELLNAME}.lef"
    else
        echo -e "Directory $TARGETDIR exists.\n\n Define another directory with -d or use -f to force overwrite.\n"
        exit 1;
    fi
fi
mkdir -p $TARGETDIR

if [ -z "$LIBNAME" ]; then
    echo "Virtuoso library not given"
    help_f;
    exit 1
fi
if [ -z "$CELLNAME" ]; then
    echo "Cell not given"
    help_f;
    exit 1
fi

. ${LEFGEN_FILES_DIR}/gen_abstract_replay.sh

echo "Generating LEF"
abstract -nogui -64 -replay "${TARGETDIR}/abstract.replay" 
if [ -f "${CELLNAME}.lef" ]; then
    mv ${CELLNAME}.lef ${TARGETDIR}/${CELLNAME}.lef
    echo "LEF generated!"
fi
