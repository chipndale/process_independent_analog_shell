#!/bin/sh
#############################################################################
# This is a design import script from digital flow to Cadence Virtuoso 
# design environment.
# The purpose is to generate all needed files
# in parametrized manner from this single file
# Created by Marko Kosunen on 24.03.2015
# Last modification by Marko Kosunen, marko.kosunen@aalto.fi, 12.10.2018 19:13
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
SCRIPTNAME="subckt_sortpins"
cat << EOF
${SCRIPTNAME} Release 1.0 (12.09.2018)
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])-Sorts the pins of a Spice subcircuit
Written by Marko "Pikkis" Kosunen"

SYNOPSIS
$(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:])  [OPTIONS]
DESCRIPTION
    Sorts the pins of a subcircuit in spice file to correspond the default output 
    order of Cadnce si netlister.
    Delault is Alphabetical names, little endian buses , bus edlimiter <>
OPTIONS"
  -d 
      Destination path
  -s
      Source path
  -S   
      Subcircuit name
  -h
      Show this help.
EOF
}
DESTFILE=""
SUBCKTNAME=""
SOURCEFILE=""
while getopts d:s:S:h opt
do
  case "$opt" in
    d) DESTFILE=${OPTARG};;
    s) SOURCEFILE=${OPTARG};;
    S) SUBCKTNAME=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done
if [ -z ${SUBCKTNAME} ]; then
    echo -e "ERROR: SUBCKTNAME not given\n"
    help_f;
    exit 1
fi
if [ -z ${SOURCEFILE} ]; then
    echo -e "ERROR: Source file not given\n"
    help_f;
    exit 1
fi
if [ -z ${DESTFILE} ]; then
    echo -e "ERROR:Destination file not given\n"
    help_f;
    exit 1
fi

#Find the subcircuit definition and store it
SUBCKTDEF=$(sed -n "/^\.[s|S][u|U][b|B][c|C][k|K][t|T]  *${SUBCKTNAME}/{p; :loop n; p; /^+/!q; b loop}" ${SOURCEFILE} \
    | sed -e '$d' -e 'y/[]/<>/' -e 's/+//g' \
    | tr -d '\n' | sed 's/  */ /g')

#This is the line immediately after the definition
MARKERLINE=$(sed -n "/^\.[s|S][u|U][b|B][c|C][k|K][t|T]  *${SUBCKTNAME}/{p; :loop n; p; /^+/!q; b loop}" ${SOURCEFILE} | sed -ne '$p')

#Split the definition to begin statement and pinlist
CKTDEF=$(echo ${SUBCKTDEF} | sed -n "s/\(^\.[s|S][u|U][b|B][c|C][k|K][t|T]  *\)\(${SUBCKTNAME} \)\(.*\)/\1\2/p")
PINDEF=$(echo ${SUBCKTDEF} | sed -n "s/\(^\.[s|S][u|U][b|B][c|C][k|K][t|T]  *\)\(${SUBCKTNAME} \)\(.*\)/\3/p")

#Replace [] delimiters with <>, sort first by name, then by bus index,
# Then compose a single sorted line
SORTPINS=$(printf "%s\n" ${PINDEF} | sed 'y/<>/  /' \
    | sort -k 1,1 -d -k 2,2nr \
    | sed 's/ /</' \
    | sed 's/ $/>/' \
    | sed 's/$/ /' \
    | tr -d '\n' \
    | sed 's/ $//')

#Variables for looping
LINE="1"
COUNT="1"
STRING="${CKTDEF} ${SORTPINS}"
FIRSTLIMIT=11
LIMIT=13

# Extract everything before the subcircuit definition form the original file
sed -n "1,/^\.[s|S][u|U][b|B][c|C][k|K][t|T]  *${SUBCKTNAME}/p" ${SOURCEFILE} \
    | sed 'y/[]/<>/' \
    | sed '$d' > ${DESTFILE}

# Echo the sorted definition to destination
for WORD in ${STRING}; do
    if [ "$LINE" == "1" ]; then 
        if [ $COUNT -lt $FIRSTLIMIT ]; then
            echo -n "$WORD " >> ${DESTFILE}
            ((COUNT++))
        elif [ "$COUNT" -eq $FIRSTLIMIT ]; then
            echo "$WORD" >> ${DESTFILE}
            COUNT="0"
        fi
        ((LINE++))
    else
        if [ $COUNT -eq 0 ]; then
            echo -n "+ $WORD " >> ${DESTFILE}
            ((COUNT++))
        elif [ $COUNT -lt $FIRSTLIMIT ]; then
            echo -n "$WORD " >> ${DESTFILE}
            ((COUNT++))
        elif [ "$COUNT" -eq $FIRSTLIMIT ]; then
            echo "$WORD" >> ${DESTFILE}
            COUNT="0"
        fi
    fi
done
# Add a linebreak, and push rest of the original file after the sorted subcircuit definition
echo "" >> ${DESTFILE}
sed -n "/^${MARKERLINE}/"',$p' ${SOURCEFILE} | sed  'y/[]/<>/' >> ${DESTFILE}

#Bye bye
exit 0

