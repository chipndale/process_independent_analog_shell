#!/bin/sh
#############################################################################
# Imports "Analog macro package" to Virtuoso
# This is the method to deliver analog macros for top level layout composition.
# written by Marko Kosunen, 08/24/2018
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the command line

help_f()
{
cat << EOF
DESCRIPTION
    Import a macro package to be used in Cadence Virtuoso. Path to the
    macro package must be supplied with the -p option.

OPTIONS"
  -f
      Force target directory overwrite (no further arguments)
  -l
      Optional rename of the target library. Useful for example when comparing macro versions.
      Default: package name from -p (without extension)
  -p
      tbz2-packed Virtuoso design database. 
  -w
      Virtuoso root directory. Should contain cds.lib file.
  -h
      Show this help.
EOF
}

FORCE="0"
VLIBOVERRIDE="0"
WORKDIR="$(pwd)"
while getopts fl:p:w:h opt
do
  case "$opt" in
    f) FORCE="1";;
    l) VIRTUOSOLIB="${OPTARG}";;
    p) PACKAGE=$(readlink -f ${OPTARG});;
    w) WORKDIR=$(readlink -f ${OPTARG});;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

if [ -z "$PACKAGE" ]; then
    echo "Please supply package option to specify the macro you are importing!"
    help_f;
    exit 1
fi

if [ ! -f "${WORKDIR}/cds.lib" ]; then
    echo "WORKDIR missing cds.lib. It is not a Virtuoso working directory."
    help_f;
    exit 1
fi

# Check if target libname was given
if [ -z "$VIRTUOSOLIB" ] || [ "$VIRTUOSOLIB" == "$(basename $PACKAGE .tbz2)" ]; then
    VIRTUOSOLIB=$(basename $PACKAGE .tbz2)
    echo "Using default macro library name from package name: ${VIRTUOSOLIB}"
else
    echo "Libname specifed. Import macro to library: ${VIRTUOSOLIB}"
    VLIBOVERRIDE="1"
fi

if [ -d "${WORKDIR}/${VIRTUOSOLIB}" ]; then
    if [ "$FORCE" == "0" ]; then
        echo "Package target directory already exists"
        echo "Use -f to force overwrite"
        exit 1
    elif [ "$FORCE" == "1" ]; then
        rm -rf "${WORKDIR}/${VIRTUOSOLIB}"
    fi
fi

#Unpack
cd ${WORKDIR}
if [ ${VLIBOVERRIDE} == "0" ]; then
    echo "Unpacking $PACKAGE to ${WORKDIR}/${VIRTUOSOLIB} ..."
    tar -C ${WORKDIR} -xjf "$PACKAGE"
else
    echo "Unpacking $PACKAGE to user-defined ${WORKDIR}/${VIRTUOSOLIB} ..."
    mkdir "${WORKDIR}/${VIRTUOSOLIB}"
    tar -C "${WORKDIR}/${VIRTUOSOLIB}" -xjf "$PACKAGE" --strip-components 1
fi

chmod g+rwxs ${VIRTUOSOLIB} 
find ./${VIRTUOSOLIB} -type d -exec chmod g+rwsx {} \;
find ./${VIRTUOSOLIB} -type f -exec chmod g+rw {} \;

if [ -z "$(sed -n "/^DEFINE \<$VIRTUOSOLIB\>/p" ./cds.lib)" ]; then
    echo "DEFINE $VIRTUOSOLIB $VIRTUOSOLIB" >> ./cds.lib 
fi
exit 0

