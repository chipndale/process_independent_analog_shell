#!/bin/sh

help_f()
{
cat << EOF
${SCRIPTNAME} - Virtuoso layout to vector graphics

Written by Okko Järvinen, 2020

SYNOPSIS
    $(echo ${SCRIPTNAME} | tr [:upper:] [:lower:]) [OPTIONS]

DESCRIPTION
    Parses Virtuoso layout layer-by-layer and exports all shapes to individual
    SVG files. The layer SVGs are parsed to one combined SVG+PDF file, and a
    PNG file if requested. The layer coloring is parsed from display.drf -file.

OPTIONS
    -a
        Display layout area in the PDF/SVG files (no PNG yet). Default off.
    -b
        Background color of the PNG image as a hex "#ffffff" or rgb
        "rgb(255,255,255)" string. Default off (transparent background).
    -c
        Cell name.   
    -d  
        Target directory. Results will be stored here. Default
        "<working_dir>/layout2fig/<lib name>/<cell name>/<view name>".
    -D  
        DPI setting used for PNG export. Adjust this to get a suitable PNG size
        depending on the layout size. Default 150.
    -f
        Force result overwriting.
    -F  
        List of formats (PNG, PDF or EPS) for the combined layout image. Default
        "PNG PDF EPS".
    -l
        Virtuoso library name.
    -L
        List of layer names to be displayed, e.g. "M1 M2 VIA1 M3 VIA2 M4". This
        defines the order of drawing in the figure. Recommended to put vias one
        layer higher than normal, so they sit on top of the metal in the figure
        (e.g. VIA1 sitting on top of second metal in above example). Default "".
    -m
        Disable shape merging. Shapes on one layer are merged by default to
        make the end result cleaner, and to reduce SVG size. This flag will
        disable merging.
    -p
        Preserve the individual SVG files for each layer.
    -P
        List of layer purposes to be included, e.g. "drawing dummy". Default "".
    -r
        List of libraries to be excluded from the temporary library. At least
        the tech library should be in this list to avoid problems with
        capacitors etc.
    -s  
        Disable outline stroke for the shapes. The stroke looks nicer for
        smaller layouts but messy for larger layouts. By default stroke is
        enabled.
    -t  
        Enable transparency for fill colors. Enabling transparency causes EPS
        format to render as bitmap instead of vector. By default transparency
        is disabled.
    -v
        View name. Default "layout".
    -w 
        Working directory. Must contain cds.lib file. Default: current
        directory.
    -h
        Show this help.
EOF
}
SCRIPTNAME="layout2fig"
WORKDIR=`pwd`
CELLNAME=""
LIBNAME=""
VIEWNAME="layout"
FORCE="0"
DPI=150
BGCOLOR=""
TP=""
LAYERLIST=""
PURPOSELIST=""
REFLIBS=""
PRESERVE_SVG=0
SVGSCALE="(2000.0/max(cvWidth cvHeight))"
STROKEWIDTH="svgScale/150.0"
STROKEWIDTH="max(cvWidth cvHeight)/1000"
STROKEWIDTH="0.1"
STROKEOPACITY="1.0"
FILLOPACITY="1.0"
FORMATS="PNG PDF EPS"
TMPLIBNAME="${SCRIPTNAME}_templib"
MERGE="1"
AREA="0"

while getopts ab:c:d:D:fF:l:L:mpP:r:stv:w:h opt
do
  case "$opt" in
    a) AREA="1";;
    b) BGCOLOR=${OPTARG};;
    c) CELLNAME=${OPTARG};; 
    d) TARGETDIR=${OPTARG};;
    D) DPI=$OPTARG;;
    f) FORCE="1";;    
    F) FORMATS=${OPTARG};;
    l) LIBNAME=${OPTARG};;
    L) LAYERLIST=${OPTARG};;
    m) MERGE="0";;
    p) PRESERVE_SVG=1;;
    P) PURPOSELIST=${OPTARG};;
    r) REFLIBS=${OPTARG};;
    s) STROKEOPACITY="0.0";;
    t) FILLOPACITY="0.85";;
    v) VIEWNAME=${OPTARG};;
    w) WORKDIR=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

if [ -z "$CELLNAME" ]; then
    echo "No cellname given!"
    exit 1
else
    CELLSTR="?cell \"${CELLNAME}\""
fi
if [ -z "$LIBNAME" ]; then
    echo "No libname given!"
    exit 1
else
    LIBSTR="?lib \"${LIBNAME}\""
fi
if [ -z "$LAYERLIST" ]; then
    echo "No layer list given!"
    exit 1
fi
if [ -z "$PURPOSELIST" ]; then
    echo "No purpose list given!"
    exit 1
fi
if [[ ! -f "./cds.lib" ]]; then
    echo "No ./cds.lib found. Run the script in a valid Virtuoso directory."
    exit 1
fi
if [[ ! -f "./display.drf" ]]; then
    echo "No ./display.drf found. Run the script in a valid Virtuoso directory."
    exit 1
fi
[[ ! -x `command -v virtuoso` ]] && echo "ERROR: Couldn't find 'virtuoso' binary." && exit 1

if [ ! -z "$BGCOLOR" ]; then
    TP="-background ${BGCOLOR}"
fi

# Create the layout2fig directory
if [ -z "$TARGETDIR" ]; then
    TARGETDIR="${WORKDIR}/layout2fig/${LIBNAME}/${CELLNAME}/${VIEWNAME}"
fi
if [ -d "$TARGETDIR" ]; then
    if [ "$FORCE" == "1" ]; then
        rm -f ${TARGETDIR}/*
    else
        echo -e "Directory $TARGETDIR exists.\n\n Define another directory with -d or use -f to force overwrite.\n"
        exit 1
    fi
fi
mkdir -p $TARGETDIR

OUTLINEFILL=$(echo "1-${FILLOPACITY}" |bc -l)

#Copy the library 
CDS_COPY_AUTO_RENAME=yes
export CDS_COPY_AUTO_RENAME
#Parse the reflibs list to Skill format
RFSTR="list( "
for lib in $REFLIBS; do
    RFSTR="$RFSTR \"$lib\""
done
RFSTR="$RFSTR )"

PROCESSFILE="${TARGETDIR}/layer2svg.il"
cat << EOF > "${PROCESSFILE}"
procedure(layer2svg()

libName=getShellEnvVar("libName")
cellName=getShellEnvVar("cellName")
viewName=getShellEnvVar("viewName")
layerName=getShellEnvVar("layerName")
styleFile = "${WORKDIR}/display.drf"

unless(cv = dbOpenCellViewByType(libName cellName viewName "maskLayout" "r")
    error("Could not open temp layout, something went wrong with copying.")
    exit(1)
)

x1cv = caar(cv~>bBox) 
x2cv = caadr(cv~>bBox) 
y1cv = cadar(cv~>bBox) 
y2cv = cadadr(cv~>bBox)
cvWidth = x2cv-x1cv
cvHeight = y2cv-y1cv
svgScale = ${SVGSCALE}


layerList = parseString("${LAYERLIST}" " ")
purposeList = parseString("${PURPOSELIST}" " ")

merge = ${MERGE}
if(merge == 1 then
    unless(cvOut = dbOpenCellViewByType(libName sprintf(nil "%s_%s" cellName layerName) viewName "maskLayout" "w")
        error("Could not open layer layout, something went wrong with copying.")
        exit(1)
    )
    printf("Merging...\n")
    prelen = length(cv~>shapes)
)
shapeList = '()
foreach(p purposeList
    lpp = car(setof(lpp cv~>lpps lpp~>layerName==layerName && lpp~>purpose==p))
    if(lpp != nil then
        if(merge == 0 then
            shapeList = append(shapeList lpp~>shapes)
        else
            foreach(shape lpp~>shapes
                dbCopyFig(shape cvOut)
            )
            lpp = car(setof(lpp cvOut~>lpps lpp~>layerName==layerName && lpp~>purpose==p))
            origShapes = lpp~>shapes
            printf("Layer: %s - %s, %d shapes\n" lpp~>layerName lpp~>purpose length(origShapes))
            shapes = dbLayerOr(cvOut list(lpp~>layerName lpp~>purpose) origShapes)
            printf("-> %d shapes\n" length(shapes))
            shapeList = append(shapeList shapes)
            foreach(shape origShapes
                dbDeleteObject(shape)
            )
            dbSave(cvOut)
        )
    )
)
if(merge == 1 then
    printf("Done. Shapes: %d -> %d.\n" prelen length(shapeList))
)
if(length(shapeList) == 0 then
    printf("No shapes on %s." layerName)
    exit(1)
)
printf("Processing %s.\n" layerName)
svgFile = strcat("${TARGETDIR}" "/" cellName "_" layerName ".svg")
file = outfile(svgFile)
offsets = printHeader(file cv)
printStyle(file styleFile layerList purposeList)
foreach(s shapeList
    cond(
        (s~>objType == "rect"
            printRect(file s cv car(offsets) cadr(offsets) cvWidth svgScale*cvHeight)
        )
        (s~>objType == "polygon"
            printPolygon(file s cv car(offsets) cadr(offsets) cvWidth svgScale*cvHeight)
        )
        (s~>objType == "pathSeg"
            sp = dbConvertPathSegToPolygon(s)
            printPolygon(file sp cv car(offsets) cadr(offsets) cvWidth svgScale*cvHeight)
        )
        (s~>objType == "path"
            sp = dbConvertPathToPolygon(s)
            printPolygon(file sp cv car(offsets) cadr(offsets) cvWidth svgScale*cvHeight)
        )
    )
)
fprintf(file "</svg>\n")
close(file)
dbClose(cv)
if(merge == 1 then
    dbClose(cvOut)
)
) ; end procedure

procedure(printHeader(file cell)
    let( (x1 x2 y1 y2 width height)
        x1 = caar(cell~>bBox)*svgScale
        x2 = caadr(cell~>bBox)*svgScale
        y1 = cadar(cell~>bBox)*svgScale
        y2 = cadadr(cell~>bBox)*svgScale
        xOffs = x1
        yOffs = y1
        width = x2-x1
        height = y2-y1
    	fprintf(file "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n")
    	fprintf(file "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n")
    	fprintf(file "<svg\n")
    	fprintf(file "	xmlns=\"http://www.w3.org/2000/svg\"\n")
    	fprintf(file "	xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n")
    	fprintf(file "	version=\"1.0\"\n")
    	fprintf(file "	width=\"%gpx\"\n" width)
    	fprintf(file "	height=\"%gpx\"\n" height)
    	fprintf(file "	viewBox=\"%g %g %g %g\"\n" 0.0 0.0 x2-x1 y2-y1) 
    	;fprintf(file "	shape-rendering=\"crispEdges\"\n")
    	fprintf(file "	shape-rendering=\"auto\"\n")
    	fprintf(file "	id=\"svg2\">\n")
    	fprintf(file "<title>%s - %s - %s</title><desc></desc>\n" libName cellName viewName)
        showArea = ${AREA}
        if(showArea == 1 then
            fontSize = max((0.025*max(width height)) 60)
            lineWidth = (fontSize/20)
            markerWidth = (7*lineWidth)
            fprintf(file "<defs>\n")
            fprintf(file "  <marker id=\"startarrow\" markerWidth=\"%g\" markerHeight=\"%g\"" markerWidth markerWidth)
            fprintf(file "  refX=\"%g\" refY=\"%g\" orient=\"auto\" markerUnits=\"userSpaceOnUse\">\n" markerWidth (markerWidth/2))
            fprintf(file "    <polygon points=\"%g 0, %g %g, 0 %g\"/>" markerWidth markerWidth markerWidth (markerWidth/2))
            fprintf(file "  </marker>\n")
            fprintf(file "  <marker id=\"stoparrow\" markerWidth=\"%g\" markerHeight=\"%g\"" markerWidth markerWidth)
            fprintf(file "  refX=\"0\" refY=\"%g\" orient=\"auto\" markerUnits=\"userSpaceOnUse\">\n" (markerWidth/2))
            fprintf(file "    <polygon points=\"0 0, %g %g, 0 %g\"/>\n" markerWidth (markerWidth/2) markerWidth)
            fprintf(file "  </marker>\n")
            fprintf(file "</defs>\n")
            fprintf(file "<line x1=\"%g\" y1=\"%g\" x2=\"%g\" y2=\"%g\" marker-start=\"url(#startarrow)\" marker-end=\"url(#stoparrow)\" stroke=\"black\" stroke-width=\"%g\"/>\n" 
                (x1+markerWidth) (y2+markerWidth-yOffs) (x2-markerWidth) (y2+markerWidth-yOffs) lineWidth)
            fprintf(file "<line x1=\"%g\" y1=\"%g\" x2=\"%g\" y2=\"%g\" marker-start=\"url(#startarrow)\" marker-end=\"url(#stoparrow)\" stroke=\"black\" stroke-width=\"%g\"/>\n" 
                (x1-markerWidth) (y1+markerWidth-yOffs) (x1-markerWidth) (y2-markerWidth-yOffs) lineWidth)
            fprintf(file "<text x=\"%g\" y=\"%g\" text-anchor=\"middle\" font-size=\"%g\" dominant-baseline=\"hanging\">%.03g µm</text>\n" ((x2-x1)/2.0) (y2+2.0*markerWidth-yOffs) fontSize (width/svgScale))
            fprintf(file "<text text-anchor=\"middle\" transform=\"translate(%g %g) rotate(270)\" font-size=\"%g\" dominant-baseline=\"auto\">%.03g µm</text>\n" (x1-2.0*markerWidth) ((y2-y1)/2.0-yOffs) fontSize (height/svgScale))
        )
        list(xOffs yOffs)
    )
) ; end procedure

procedure(printRect(file shape cell xOffs yOffs width height)
    let( (x1 x2 y1 y2)
        x1 = svgScale*caar(shape~>bBox)-xOffs
        x2 = svgScale*caadr(shape~>bBox)-xOffs 
        y1 = -svgScale*cadar(shape~>bBox)+height+yOffs
        y2 = -svgScale*cadadr(shape~>bBox)+height+yOffs
    	fprintf(file "<polygon class=\"%s_%s\" points=\"%g,%g %g,%g %g,%g %g,%g %g,%g\"/>\n" 
    		shape~>layerName shape~>purpose x1 y1 x2 y1 x2 y2 x1 y2 x1 y1)
    )
) ; end procedure

procedure(printPolygon(file shape cell xOffs yOffs width height)
    let( (points x y)
    	fprintf(file "<polygon class=\"%s_%s\" points=\"" shape~>layerName shape~>purpose)
    	points = shape~>path
    	(foreach p points 
            x = svgScale*xCoord(p)-xOffs
            y = -svgScale*yCoord(p)+height+yOffs
    		fprintf(file "%g,%g " x y)
    	)
    	fprintf(file "\"/>\n")
   	)
) ; end procedure

procedure(printStyle(file styleFile layerList purposeList)
    prog( (inFile tokens lname isColor isLayer fill fillstyle layer purpose)
    	fprintf(file "<style>\n")
        unless(inFile = infile(styleFile)
            fprintf(file "</style>\n")
            error("Could not open display.drf.")
            return(nil)
        )
        isColor = 0
        isLayer = 0
        colorTable = makeTable('dataTable nil)
        while(gets(line inFile)
            tokens = parseString(line)
            if(length(tokens) > 0 && substring(nthelem(1 tokens) 1 1) != ";" then
                if(nthelem(1 tokens) == "drDefineColor(" then
                    isColor = 1
                )
                if(isColor == 1 && nthelem(1 tokens) == ")" then
                    isColor = 0
                )
                if(isColor == 0 && nthelem(1 tokens) == "drDefinePacket(" then
                    isLayer = 1
                )
                if(isLayer == 1 && nthelem(1 tokens) == ")" then
                    isLayer = 0
                )
                lname = nthelem(3 tokens)
                if(lname != nil && isLayer == 0 && isColor == 1 then
                    colorTable[lname] = sprintf(nil "rgb(%s,%s,%s)" nthelem(4 tokens) nthelem(5 tokens) nthelem(6 tokens))
                )
                if(lname != nil && isColor == 0 && isLayer == 1 then
                    layer = car(parseString(lname "_"))
                    purpose = cadr(parseString(lname "_"))
                    if(member(layer layerList) && member(purpose purposeList) then
                        fprintf(file ".%s {" lname)
                        fill = nthelem(6 tokens)
                        if(fill != nil then
                            fprintf(file "fill:%s;" colorTable[fill])
                        )
                        fillstyle = nthelem(8 tokens)
                        if(fillstyle != nil then
                            if(fillstyle == "outline" ||fillstyle == "X" then
                                fprintf(file "fill-opacity:${OUTLINEFILL};")
                                fprintf(file "stroke-opacity:1.0;")
                                fprintf(file "stroke-width:%g;" ${STROKEWIDTH})
                                fprintf(file "stroke:%s;" colorTable[fill])
                                fprintf(file "vector-effect:non-scaling-stroke;")
                            else
                                fprintf(file "fill-opacity:${FILLOPACITY};")
                                fprintf(file "stroke-opacity:${STROKEOPACITY};")
                                fprintf(file "stroke-width:%g;" ${STROKEWIDTH})
                                ;fprintf(file "stroke:%s;" colorTable[fill])
                                fprintf(file "stroke:%s;" "#000000")
                                fprintf(file "vector-effect:non-scaling-stroke;")
                            )
                        )
                        fprintf(file "}\n")
                    )
                ) 
            )
        )
        close(inFile)
    	fprintf(file "</style>\n")
   	)
) ; end procedure
layer2svg()
EOF

SKILLFILE="${TARGETDIR}/layout2fig_main.il"
cat << EOF > "${SKILLFILE}"
_globalDistributeSKILLTable=makeTable("_globalDistributeSKILLTable" nil)
_globalDistributeSKILLTable["childIdList"]=nil
_globalDistributeSKILLTable["layerList"]=nil

procedure(layout2fig()

libName = "${LIBNAME}"
cellName = "${CELLNAME}"
viewName = "${VIEWNAME}"
rootPath = "${WORKDIR}"
layerStr = "${LAYERLIST}"
purposeStr = "${PURPOSELIST}"
tempLibName = "${TMPLIBNAME}"
styleFile = "${WORKDIR}/display.drf"

unless(cvOrig = dbOpenCellViewByType(libName cellName viewName "maskLayout" "r")
    error("Could not open layout, please check library and cell.")
)

; Create a temporary library with the correct technology library name
printf("Creating temporary library %s.\n" tempLibName)
techName = techGetTechLibName(cvOrig) 
unless(tempLib = ddGetObj(tempLibName)
    tempLib = dbCreateLib(tempLibName)
)
techSetTechLibName(tempLib techName)

src_layout=gdmCreateSpec("${LIBNAME}" "${CELLNAME}" "${VIEWNAME}" "" "CDBA" )
dest=gdmCreateSpec(tempLibName "" "" "" "CDBA" )
reflibs = $RFSTR
skiplist=gdmCreateSpecList()
foreach(lib reflibs \
    lib=gdmCreateSpec(lib "" "" "" "CDBA" ) \
    gdmAddSpecToSpecList(lib skiplist) \
)
ccpCopyExactDesign(src_layout dest t 'CCP_EXPAND_ALL skiplist nil list("${VIEWNAME}" "auCdl") "" "CDBA" 'CCP_UPDATE_COPIED_DATA ) 
unless(cv = dbOpenCellViewByType(tempLibName cellName viewName "maskLayout" "a")
    error("Could not open temp layout, something went wrong with copying.")
)
dbClose(cvOrig)
printf("Temporary library generated.\n")

foreach(via cv~>vias
    dbFlattenInst(via 1)
)
foreach(inst cv~>instances
    dbFlattenInst(inst 32 t nil nil nil t nil t)
)
; Deleting pins so merging will work properly
preservePins = 0
if(preservePins == 0 then
    foreach(s cv~>shapes
        if(s~>pin then
            net = s~>pin~>net
            dbDeleteObject(s~>pin)
            dbAddFigToNet(s net)
        )
    )
)
printf("Done flattening.\n")
dbSave(cv)
dbClose(cv)

layerList = parseString(layerStr " ")
_globalDistributeSKILLTable["layerList"]=layerList
foreach(l layerList
    cmd1=sprintf(nil "export startDistribution=yes")
    cmd2=sprintf(nil "export libName=%s" tempLibName)
    cmd3=sprintf(nil "export cellName=%s" cellName)
    cmd4=sprintf(nil "export viewName=%s" viewName)
    cmd5=sprintf(nil "export layerName=%s" l)
    cmd6=sprintf(nil "virtuoso -nograph -replay ${PROCESSFILE} -log ${TARGETDIR}/child_%s.log &" l)
    cmd=buildString(list(cmd1 cmd2 cmd3 cmd4 cmd5 cmd6) ";")
    child=ipcBeginProcess(cmd "" nil nil "postFunc")
    _globalDistributeSKILLTable["childIdList"]=cons(child _globalDistributeSKILLTable["childIdList"])
    printf("Launching child process (%L) for layer %s\n" child l)
)
) ; end procedure

procedure( postFunc(childID exitStatus)
    let( (layer childList)
        printf("Child %L done (Status %L). Remaining: %d.\n" childID exitStatus (length(_globalDistributeSKILLTable["layerList"])-1))
        if( length(_globalDistributeSKILLTable["layerList"])>1 then
            layer=car(_globalDistributeSKILLTable["layerList"])
            _globalDistributeSKILLTable["layerList"]=remove(layer _globalDistributeSKILLTable["layerList"])
        else
            childList=setof(x _globalDistributeSKILLTable["childIdList"] ipcIsAliveProcess(x))
            unless( childList
                if( libId = ddGetObj(tempLibName) then
                    ddDeleteObj(libId)
                    printf("%s has been deleted.\n" tempLibName)
                else
                    error("Lib could not be deleted.")
                )
                exit()
            )
        ) ;if
    ) ;let
) ; end procedure
layout2fig()
EOF

echo "Parsing layout..."
virtuoso -nograph -restore ${SKILLFILE}
echo "Done!"
echo "Generating images..."
BASENAME=${TARGETDIR}/${CELLNAME}
# Combine layer SVGs to one SVG
COMBSVG=${TARGETDIR}/${CELLNAME}.svg
if [[ -f "${COMBSVG}" ]]; then
    rm -f ${COMBSVG}
fi
FIRSTSVG=$(ls ${TARGETDIR}/*.svg |head -1)
echo "Generating ${BASENAME}.svg"
sed '/<\/style>/q' ${FIRSTSVG} > ${COMBSVG}
SVGFILES=""
for layer in ${LAYERLIST}; do
    SVGNAME="${BASENAME}_${layer}.svg"
    if [[ -f "${SVGNAME}" ]]; then
        SVGFILES+="${SVGNAME} "
        sed -n '/^<\/style>/,/^<\/svg>/{/^<\/style>/!{/^<\/svg>/!p;};}' ${SVGNAME} >> ${COMBSVG}
    fi
done
echo "</svg>" >> ${COMBSVG}
# Generate PDF from combined SVG
if [[ ${FORMATS} == *"PDF"*  ]]; then
    echo "Generating ${BASENAME}.pdf"
    inkscape -z -D -A ${BASENAME}.pdf ${BASENAME}.svg
fi
if [[ ${FORMATS} == *"EPS"*  ]]; then
    echo "Generating ${BASENAME}.eps"
    inkscape -z -D -E ${BASENAME}.eps ${BASENAME}.svg
fi
# Generate PNG from layer SVGs
if [[ ${FORMATS} == *"PNG"*  ]]; then
    echo "Generating ${BASENAME}.png"
    convert -quality 100 -density $DPI -background none ${SVGFILES} ${TP} -flatten ${BASENAME}.png
fi
# Remove layer SVGs
if [ $PRESERVE_SVG == 0 ]; then
    for file in ${SVGFILES}; do
        echo "Removing ${file}"
        rm -f ${file}
    done
else
    echo "Preserving layer SVG files."
fi
# Clean script and log files
rm -f ${SKILLFILE}
rm -f ${PROCESSFILE}
rm -f ${TARGETDIR}/*.log
# Clean temporary library from cds.lib
echo "Cleaning temporary library from ./cds.lib"
cp ./cds.lib ./cds.lib.rl_backup
grep -v "${TMPLIBNAME}" ./cds.lib > tmpcdslib; mv tmpcdslib ./cds.lib
echo "Done!"
exit 0
