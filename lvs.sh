#!/bin/sh
#############################################################################
# This is a script for running lvs for Virtuoso designs
#
# Created by Marko Kosunen on 24.03.2015
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
SCRIPTNAME="lvs"
cat << EOF
${SCRIPTNAME} Release 1.0 (23.08.2018)
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])-run lvs on virtuoso cell"
Written by Marko "Pikkis" Kosunen"

SYNOPSIS
$(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:])  [OPTIONS]
DESCRIPTION
    Runs lvs on Virtuoso cell with schematic and layout view's present.

OPTIONS
  -c
      Top cell name.   
  -C  
      SVRF command file. used for example to define blackboxes and filters
  -d  
      LVS run directory. Results will be stored here. Default 
      <working dir>/<Top cell name>_lvs_run  
  -D
      Directive [string]. Custom string to be passed to
      process and tool spesific check defintions for further
      control. Default \"\"
  -f
      Force result overwriting
  -g
      GDS file name. Use this file as layout instead of the 'layout' view
  -G 
      Ground net name list "STRING" default "VSS"
  -H
      hcells file name
  -l
      Virtuoso library name.
  -i
      Enable result browsing after running LVS.
  -r
      Reference spice netlist file list "STRING"
  -s  
      Use this spice netlist, Do not extract.
  -S  
      Supply net list "STRING", Default "VDD"
  -t
     Technology library.
  -v 
     Nets for virtual connect of pins. "String String", default empty.
  -V 
     Nets for virtual connect of BLACK BOX pins. "String String", default empty.
  -w 
     Working directory. Must contain cds.lib file is netlists of GDS are generated
     from Virtuoso library. Default: current directory.
  -h
      Show this help.
EOF
}

#This is a technology dependent includefile for spice netlisting.
. ${LVS_FILES_DIR}/lvs_variables.sh

WORKDIR=`pwd`
CELL=""
VIRTUOSOTARGETLIB=""
HCELLSARG=""
REFLIBS=""
FORCE="0"
TECHLIB=""
SPICE=""
GENSPICE="0"
GDS=""
GENGDS="0"
GNDNETS="VSS"
SUPNETS="VDD"
RUNID=""
VIRTUALCNETS=""
VIRTUALBOXNETS=""
DIRECTIVE=""
INTERACTIVE="0"

while getopts c:C:d:D:fg:G:H:il:r:s:S:t:v:V:w:h opt
do
  case "$opt" in
    c) CELL=${OPTARG};; 
    C) SVRFCMDFILE=${OPTARG};;
    d) LVS_RUN_DIR=`readlink -f ${OPTARG}`;;
    D) DIRECTIVE=${OPTARG};;
    f) FORCE="1";;
    g) GDS=${OPTARG};;
    G) GNDNETS=${OPTARG};;
    H) HCELLSARG=" -hcell ${OPTARG}";;
    l) VIRTUOSOTARGETLIB=${OPTARG};;
    i) INTERACTIVE="1";;
    r) REFLIBS=${OPTARG};;
    s) SPICE=${OPTARG};;
    S) SUPNETS=${OPTARG};;
    t) TECHLIB=${OPTARG};;
    V) VIRTUALBOXNETS=${OPTARG};;
    v) VIRTUALCNETS=${OPTARG};;
    w) WORKDIR=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

#Create the lvs run directory   
if [ -z "$LVS_RUN_DIR" ]; then
    LVS_RUN_DIR="${WORKDIR}/${CELL}_lvs_run"
fi

if [ -d "$LVS_RUN_DIR" ]; then
    if [ "$FORCE" == "1" ]; then
        rm -rf "$LVS_RUN_DIR" 
    else
        echo -e "Directory $LVS_RUN_DIR exists.\n\n Define another directory with -d or use -f to force overwrite.\n"
        exit 1;
    fi
fi
mkdir -p $LVS_RUN_DIR

if [ -z "$VIRTUOSOTARGETLIB" ]; then
    echo "Virtuoso library not given"
    help_f;
    exit 1
fi
if [ -z "$CELL" ]; then
    echo "Cell not given library not given"
    help_f;
    exit 1
fi

if [ -z "$TECHLIB" ]; then
    echo "Technology library not given"
    help_f;
    exit 1
fi

if [ -z "$GDS" ]; then
    #This is the default GDS  extracted from the layout
    #Otherwise use give file
    GENGDS="1"
    GDS=${LVS_RUN_DIR}/${CELL}.calibre.db
elif [ ! -f "$GDS" ]; then
    echo "GDS file $GDS does not exist"
    exit 1
fi

GENSPICE="0"
if [ -z "$SPICE" ]; then
    #This is the default SPICE  extracted from the schematic
    #Otherwise use given file
    GENSPICE="1"
    SPICE=${CELL}.src.net
elif [ ! -f "$SPICE" ]; then
    echo "SPICE file $SPICE does not exist"
    exit 1
fi


echo "LVS_RUN_DIR set to ${LVS_RUN_DIR}" \
    | tee -a ${LVS_RUN_DIR}/${CELL}.lvs_run.log
echo "Start logging to ${LVS_RUN_DIR}/$CELL.lvs_run.log" \
    | tee -a ${LVS_RUN_DIR}/${CELL}.lvs_run.log

# Exporting netlist from virtuoso schematics:
# Run from virtuoso folder (si.env should be there):
if [ "$GENSPICE" == "1" ]; then

CURRENTFILE="${WORKDIR}"/si.env
cat << EOF > "${CURRENTFILE}"
simStopList = '("auCdl")
simViewList = '("auCdl" "schematic")
globalGndSig = ""
globalPowerSig = ""
shrinkFACTOR = 0.0
checkScale = "meter"
preserveDIO = 't
checkDIOAREA = 't
checkDIOPERI = 't
preserveCAP = 't
checkCAPVAL = 't
checkCAPAREA = 'nil
checkCAPPERI = 'nil
checkLDD = 'nil
preserveRES = 't
checkRESVAL = 't
checkRESSIZE ='nil
resistorModel = ""
shortRES = 2000
simNetlistHier = 't
pinMAP = 'nil
displayPININFO = 't
preserveALL = 't
preserveBangInNetlist = 'nil
checkLDD = 'nil
connects = ""
setEQUIV = ""
simRunDir = "$WORKDIR"
hnlNetlistFileName = "${SPICE}"
incFILE = "$INCFILE"
simSimulator = "auCdl"
simViewName = "schematic"
simCellName = "${CELL}"
simLibName = "${VIRTUOSOTARGETLIB}"
simNotIncremental = 'nil
simPrintInhConnAttributes = 'nil
simReNetlistAll = 'nil
cdlSimViewList = '("auCdl" "schematic")
cdlSimStopList = '("auCdl")
EOF
    cd ${WORKDIR}
    #delete .running before running this
    echo "Generating netlist..." \
    | tee -a ${LVS_RUN_DIR}/${CELL}.lvs_run.log
    rm -f .running && si -batch -command netlist \
        | tee ${LVS_RUN_DIR}/${CELL}.lvs_run.log
    mv ${SPICE} ${LVS_RUN_DIR}/ 
fi

if [ "${GENSPICE}" == "1" ] && [ ! -f "${LVS_RUN_DIR}/${SPICE}" ]; then
     echo "Netlist generation failed! Exiting..." \
        | tee ${LVS_RUN_DIR}/${CELL}.lvs_run.log
     exit 1
fi

echo "Exporting GDS..." \
| tee -a ${LVS_RUN_DIR}/${CELL}.lvs_run.log

#Export gds from virtuoso layout:
if [ "$GENGDS" == "1" ]; then
    cd ${WORKDIR}
    echo "Running: strmout -rundir ${WORKDIR} -outputDir ${LVS_RUN_DIR} \
    -library ${VIRTUOSOTARGETLIB} -strmFile ${GDS} \
    -techLib ${TECHLIB} -topCell ${CELL} -view 'layout' \
    -logFile ${CELL}.strmout.log -enableColoring | tee -a ${LVS_RUN_DIR}/${CELL}.lvs_run.log 
    "
    strmout -rundir ${WORKDIR} -outputDir ${LVS_RUN_DIR} \
    -library ${VIRTUOSOTARGETLIB} -strmFile ${GDS} \
    -techLib ${TECHLIB} -topCell ${CELL} -view 'layout' \
    -logFile ${CELL}.strmout.log -enableColoring | tee -a ${LVS_RUN_DIR}/${CELL}.lvs_run.log 
fi

. ${LVS_FILES_DIR}/lvs_include.sh

# Success flag is created in lvs_include.sh
if [ ! -z "$LVS_SUCCESS_FLAG" ]; then
	touch ${LVS_RUN_DIR}/$CELL.success
    exit 0
else
    #Something nonzero for incorrect LVS
    exit 2
fi

#Final exit if we get this far
exit 0


