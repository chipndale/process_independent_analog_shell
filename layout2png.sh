#!/bin/sh

help_f()
{
SCRIPTNAME="layout2png"
cat << EOF
SYNOPSIS
    $(echo ${SCRIPTNAME} | tr [:upper:] [:lower:]) [OPTIONS]

DESCRIPTION
    Extracts PNG image from Virtuoso layout. Run this script from Virtuoso
    directory (contains csd.lib) with Virtuoso in PATH. 

OPTIONS
    -c
        Cell name.   
    -d  
        Target directory. Results will be stored here. Default
        "<working_dir>/layout2png/".
    -f
        Force result overwriting.
    -l
        Virtuoso library name.
    -L
        List of layer names to be hidden, e.g. "M1 M2 M3". Default "text".
    -m
        Monochrome output. Appends a "_m" to the filename. Default is full
        color.
    -P
        List of layer purposes to be hidden, e.g. "pin dummy". Default "pin".
    -s  
        Image scale. Default 4.
    -t
        Transparent background. Appends a "_t" to the filename. Default
        background is black.
    -w 
        Working directory. Must contain cds.lib file. Default: current
        directory.
    -h
        Show this help.
EOF
}
WORKDIR=`pwd`
CELLNAME=""
LIBNAME=""
FORCE="0"
SCALE=4
TP=""
MONO=""
LAYERLIST="text"
PURPOSELIST="pin"

while getopts c:d:fl:L:mP:s:tw:h opt
do
  case "$opt" in
    c) CELLNAME=${OPTARG};; 
    d) TARGETDIR=${OPTARG};;
    f) FORCE="1";;    
    l) LIBNAME=${OPTARG};;
    L) LAYERLIST=${OPTARG};;
    P) PURPOSELIST=${OPTARG};;
    s) SCALE=$OPTARG;;
    t) TP="?bgType 'transparent";;
    m) MONO="?colorType 'monochrome ?fgColor \"black\" ?bgColor \"white\"";;
    w) WORKDIR=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

if [ -z "$CELLNAME" ]; then
    echo "No cellname given!"
    exit 1
else
    CELLSTR="?cell \"${CELLNAME}\""
fi
if [ -z "$LIBNAME" ]; then
    echo "No libname given!"
    exit 1
else
    LIBSTR="?lib \"${LIBNAME}\""
fi

if [ "$TP" = "" ]; then
    TPSTR=""
else
    TPSTR="_t"
fi
if [ "$MONO" = "" ]; then
    MONOSTR=""
else
    MONOSTR="_m"
fi

FORCE="1"
FILENAME="${LIBNAME}_${CELLNAME}"

# Create the layout2png directory
if [ -z "$TARGETDIR" ]; then
    TARGETDIR="${WORKDIR}/layout2png"
fi
if [ -d "$TARGETDIR" ]; then
    if [ "$FORCE" == "1" ]; then
        rm -f "$TARGETDIR/${FILENAME}.png"
    else
        echo -e "Directory $TARGETDIR exists.\n\n Define another directory with -d or use -f to force overwrite.\n"
        exit 1
    fi
fi
mkdir -p $TARGETDIR
CURRENTFILE="${TARGETDIR}/layout2png_tmp.replay"
cat << EOF > "${CURRENTFILE}"
win = geOpen(
    ?lib "${LIBNAME}"
    ?cell "${CELLNAME}"
    ?view "layout"
    ?mode "r"
)
cell = geGetWindowCellView(win)
hiZoomIn(win cell~>bBox)
win->stopLevel = dbGetMaxHierDepth()
leSetAllLayerVisible(t)
foreach(lpp cell~>layerPurposePairs
    if( member(lpp~>layerName parseString("${LAYERLIST}" " ")) then
        printf("Hiding (\"%s\" \"%s\")\n" lpp~>layerName lpp~>purpose)
        leSetLayerVisible(list(lpp~>layerName lpp~>purpose) nil)
    )
    if( member(lpp~>purpose parseString("${PURPOSELIST}" " ")) then
        printf("Hiding (\"%s\" \"%s\")\n" lpp~>layerName lpp~>purpose)
        leSetLayerVisible(list(lpp~>layerName lpp~>purpose) nil)
    )
)
hiExportImage(?fileName "${TARGETDIR}/${FILENAME}${TPSTR}${MONOSTR}.png" ?window win ?bBox cell~>bBox ?scaleFactor ${SCALE} ?haloType 'simple ?windowProps list(list("drawAxesOn" nil) list("drawGridOn" nil)) ${TP} ${MONO})
exit()
EOF
echo "Generating image..."
virtuoso -replay ${CURRENTFILE}
rm -f ${CURRENTFILE}
echo "Done!"
exit 0
