#!/bin/sh
set -e
help_f()
{
SCRIPTNAME="lef2py"
cat << EOF
${SCRIPTNAME} Release 1.0 (23.08.2018)
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])-fetch pin bounding boxes from LEF and export Pythonic format"
Writen by Santeri Porrasmaa

SYNOPSIS
$(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:])  [OPTIONS]
DESCRIPTION
    Reads the given LEF file from a macro release and outputs the pin data in Pythonic format
    to given output file. Assumes that import macro package is available in the current
    working directory.
OPTIONS
  -f
      force overwrite of text file containing the pins
  -o
      name of output file. Default: stdout
  -l
      name of library to extract pin information from
  -c
      name of the cell to which pins belong to
EOF
}

overwrite="0"

while getopts c:fhl:p:o: opt
do
  case "$opt" in
    f) overwrite="1";;
    h) help_f; exit 0;;
    l) lib=${OPTARG};;
    c) cell=${OPTARG};;
    o) fname=${OPTARG};;
    \?) help_f;;
  esac
done


if [ -z "$lib" ]; then
    help_f
    echo "ERR: must specify a valib virtuoso Library"
    exit 1
fi
if [ -z "${cell}" ]; then
    help_f
    echo "ERR: must specify a valib virtuoso cell"
    exit 1
fi
leffile=$lib/digital/lef/${cell}.lef
if [ ! -f $leffile ]; then # Lib name given and it exists
    echo "LEF doesn't exist for cell $cell in lib $lib"
    exit 1
else
    # Find the pins
    if [ -z "$pins" ]; then
        pins=`grep -oP '(?<=PIN\s)\w+\[*\d*\]*' $leffile` 
        virt_pins=(`sed 's/\[\([0-9]*\)\]/\<\1\>/g' <<< "$pins"`)
        pins=`sed 's/\[/\\\[/g' <<< "$pins"`
        pins=`sed 's/\]/\\\]/g' <<< "$pins"`
    fi
    origx=`grep -oP '(?<=ORIGIN\s)[0-9]+([\.0-9]*)' $leffile`
    origy=`grep -oP "(?<=ORIGIN\s$origx\s)[0-9]+([\.0-9]*)" $leffile`
    sizex=`grep -oP '(?<=SIZE\s)[0-9]+([\.0-9]*)' $leffile`
    sizey=`grep -oP "(?<=SIZE\s$sizex\sBY\s)[0-9]+([\.0-9]*)" $leffile`
    ybound=$(awk '{print $1+$2}' <<< "${origy} ${sizey}")
    xbound=$(awk '{print $1+$2}' <<< "${origx} ${sizex}")
    echo "BBox:($origx,$origy,$xbound,$ybound)"
fi
if [ "$fname" ]; then
    #fname="${lib}_pins.txt"
    if [[ $overwrite == "0" && -f $fname ]]; then
        echo "Output file $fname exists! Please specify another output file or run with -f!"
        exit 1
    fi
    if [[ $overwrite == "1" && -f $fname ]]; then
        rm -f $fname
    fi
fi

count=0
for pin in $pins
do
    # This is a mess but works:
    # First sed: find line between pin definitions
    # second sed: find lines between port definitions
    # third sed, extract coordinates from RECT statement and place them inside brackets, separated by commas
    coords=`sed -n "/.*PIN ${pin}/,/END ${pin}/p" ${leffile} | sed -n '/.*PORT/,/.*END/p' | sed -n 's/RECT \([0-9\.]*\) \([0-9\.]*\) \([0-9\.]*\) \([0-9\.]*\) ;/(\1,\2,\3,\4)/p'`
    # Strip whitespace
    coords=`echo $coords | sed 's/\s\+//g'`
    # Add comma between pins defs (there may be several)
    coords=`echo $coords | sed 's/)(/),(/g'`
    layer=`sed -n "/.*PIN ${pin}/,/END ${pin}/p" ${leffile} | sed -n '/.*PORT/,/.*END/p' | sed -n 's/LAYER \([A-Z0-9]*\) ;/\1/p'`
    layer=`echo $layer | sed 's/\s\+//g'`
    if [ -z "$file" ]; then
        echo "${virt_pins[$count]}:$layer,$coords"
    else
        echo "${virt_pins[$count]}:$layer,$coords" >> "${fname}"
    fi
    count=$((count+1))
done
exit 0
