# PDK setup for Virtuoso

Setup the contents of the following files
 - [ ] sourceme.csh
 - [ ] configure

Objective is that after you run environment_setup/sourceme.csh fom the project
home, you end up in a dedicated virtuoso directory and by issuing command
`virtuoso`, you get a design environment with PDK technology library in place.

