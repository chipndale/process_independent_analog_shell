# Memory blocks
Locate the memory generator for the process OR create a resonable size
memory block layout and symbol for PROCESS_MEMORY_TYPE_<naddr>_<nout>_writeread
Where
 * PROCESS_MEMORY_TYPE is to be replaced with a string describing 
 the process and memory type
 * <naddr> is the number of address bits
 * <nout> is the number of output bits

Example: VENDOR_RAM_12_8_writeread.

You must follow this notation.

  - [ ] Generate two memories: 12 address bits, 8 outpu bits and 8 address
  bits 8 output bits, and make macro releases for these.
  - [ ] If you yse vendor emmory generator, link the functinal model files 
  (verilog), LEF and LIB to the corresponding directories used in ECD 
  macro release.
  :w
  - [ ] Write a cript  that does this, and with the help of 
  `environment_setup/process_independent_analog_shell/memory generator.sh, 
  organize it so that the taask is performed with a given Chisel memory mapping 
  .conf file.
  
