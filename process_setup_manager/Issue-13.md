# Chip level flow with digital import.
At the chip project level, start a clean session, run necessary sourceme's
and run `./configure && make DSKIP_spi_slave=0 import_spi_slave'

If everything runs smoothly, you should get LVS clean spi_slave from the
digital flow imported to virtuoso. You can check that with `make debug_innovus`

You amy re-run the import step again and again until you get it right.
Most like something will fail. Check the problem parts if import fails.
 . [ ] Digital libraries defined in Virtuoso
 - [ ] Digital libraries defined in configure as reference libraries.
 - [ ] Exclusion lists properly defined in digital flow.

