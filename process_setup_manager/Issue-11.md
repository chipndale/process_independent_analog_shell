# Configure place and route

After completing the logic synthesis, you should configure the necessary
variables in 'process_dependent_configs/process_variables.sh' and 
'innovus_default_config.sh'. You should define the variables fot the following 
cells:

 - [ ] Welltaps
 - [ ] Tie high and tie low
 - [ ] End caps, for rows and cornes, top and bottom row. Depending on the 
 process you may or may not need to define all of them.
 - [ ] Rowmaster. This is used to route metal layer 1 steripes over the rows.
 - [ ] Fiillers. Filler of width 1 and of width 2 must be defined separately 
  to perfor a forced fix preventing 2 adjacent width 2 fillers that in some 
  processes cause DRC errors
 - [ ] List odf cells to be excluded from the simulation and LVS netlists.
 These include cells like decaps, fillers etc that are physical only or are not 
 needed in LVS checking.

Once these are correct, you should run the flow until the end of 
floorplan with `make TO=floorplan'. Most likely it will fail first
time, and you need to edit `desing_scripts/create_rings_and_stripes.tcl`
to get a reasonably routed power mesh. Especially check that.

 - [ ] End caps are in place.
 - [ ] Well taps are in palce (if needed).
 - [ ] Metal layer 1 is routed ower the rows (ROWMASTER variable) snd
 connected to core rings.
 - [ ] Metal layer 3 stripes are properly connected to metal layer 1 row
 stripes.
 - Stripes on higher layers are routed properly ('properly' being somewhat 
 qualitatively defined.)

