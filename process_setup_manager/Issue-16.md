# Test notification

Run `make notify`. You should get notification about chip being ready. 
You can also configure a Slack webhook to send the notification to your 
favourite slack channel. For templates it is advisable use #random 
channel of your organization.

