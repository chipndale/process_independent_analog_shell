# Define timing corners and parasitic extraction parameters

Determine values for:
 - [ ] NOMINAL_CORNER_NAME 
 - [ ] SETUP_CORNERS slow, lower voltage, higher temperature. 
 - [ ] HOLD_CORNERS fast, higher voltage, lower temperature. 
 - [ ] SLOW_LIBS 
 - [ ] FAST_LIBS

The determine the values for:
 - [ ] NOMINAL_VOLTAGE
 - [ ] MIN_VOLTAGE
 - [ ] MAX_VOLTAGE
 - [ ] NOMINAL_TEMP
 - [ ] MIN_TEMP
 - [ ] MAX_TEMP
 - [ ] MINCORNER, Analysis MIN and MAX Corners Cmin | Cmax | RCmin | RCmax
 - [ ] MAXCORNER
These are to control parasitic operation corners in mmmc_config file.

Furthermore you need QRC extraction files for more accurate timing extraction
If there are not available, you can not define extracition corners with mmmc-file.
 - [ ] QRCMINRCFILE
 - [ ] QRCMINCFILE
 - [ ] QRCMAXRCFILE
 - [ ] QRCMAXCFILE

