# LVS for the digital flow
Run `make lvs DSKIP_spi_slave=0`. If it is clean, everything is OK.
When it is not, check the following.

 - [ ] Schematics of digital cells availble OR spice netlists of digital cells provided 
 OR digital cella balckboxed in LVS:BOX.txt
 - [ ] iRe-check if digital desing proprly imported to virtuoso.

