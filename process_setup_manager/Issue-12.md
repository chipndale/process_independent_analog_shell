# Place and route until postroute.
Run place and route with `./configure & make clean && make`until
the end. You should get a clean, placed and routed layout. Examine it 
with `make debug innovus`. 

 - [ ] Open the `dbs/postroute/spi_slave.enc` and 
 examine it visually.
 - [ ] Check the timig reports with `debug timing` This is the simple 
 flow, so there is currently no feedback from sta to postroute ECO step.  
 therefore we rely on this timing report. You may also check sta, but there 
 might be minor discrepancies.

