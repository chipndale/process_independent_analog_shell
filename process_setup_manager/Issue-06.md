# Create Chip template

With the padrin template segments, construct a chip schematic and layout with name 'template_wio'. 
Make it LVS and DRC clean. Release it to filesystem repository.

