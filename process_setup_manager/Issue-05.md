# Create padring template

Padring template is used to give a funtional template for padring containing
analog and digital power domains. The segments start with a corner cell, and always have origin at bottom left corner. Combining the segments should result in full padring.
 - [ ] In virtuoso, create four cells with names
   * template_ios_left
   * template_ios_top
   * template_ios_top
   * template_ios_top
   * template_ios_top
 - [ ] Create filesystem repository with name <process_name>_filesystem repository.
 - [ ] Make macro release of those cells.
 - [ ] Add these cells as submodules to the macro_repository submodule of the 
 <processname>_chip project.


 
