# Configure digital LEF and LIB libraries for digital flow

Locate the required LEF anf LIB files and modify spi_slave/process_dependent_configs_and_helpers/process_variables.sh so that
once you run `configure` under spi_slave, the correct LEF and LIB files are linked under flow/tech/lef and  flow/tech/lib.

 - [ ] tech.tlef or some other technology lef file to TECHLEF
 - [ ] GDS map file to GDS_MAP 
 - [ ] LEF-files of correct library flavor (HVT | RVT | LVT | SLVT) 
 - [ ] All LIB-files
 The files above enable flows for core structures and digital macros.

 Furthermore, in case you wish to construct your IO ring in digital domain:
 - [ ] IO LEF files
 - [ ] BUMP LEF files

Furthermore, based on the contents of the LEF-files and format of the LIB files, you should be able to determine the values for following variables

 - [ ] NODE, i.e. the process node.
 - [ ] SITE, i.e the site of the standard cells.
 - [ ] LIBSUFFIX, i.e. the suffix of the lib files. They might be gzipped.


