set digital=`grep DIGITALSUBDESIGNS ./configure \
    | sed -e 's/DIGITALSUBDESIGNS=//g' -e 's/"//g' \
        | awk '{print $1}'`

cat << EOF 
Please run he following:

source ${digital}/sourceme.csh
source environment_setup/sourceme.csh && cd ..

OBS: in this order.
These can not be sourced with a script


EOF
unset digital

