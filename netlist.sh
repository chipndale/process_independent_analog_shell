#!/bin/sh
# Immediately exit on error
set -e
#############################################################################
# This is a command line netlister script from Cadence Virtuoso 
# design environment.
# Aim is to support several netlisters with a single script
# Created by Marko Kosunen on 26.02.2019
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
SCRIPTNAME="netlist"
cat << EOF
${SCRIPTNAME} Release 1.0 (23.08.2018)
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])-run lvs on virtuoso cell"
Written by Marko "Pikkis" Kosunen"

SYNOPSIS
$(echo ${SCRIPTNAME} |  tr [:upper:] [:lower:])  [OPTIONS]
DESCRIPTION
  Netlists a design from Cadence Virtuoso desing environment  
OPTIONS
  -c
      Top cell name.   
  -d  
      Netlist run directory. Results will be stored here. Default <working dir>/ELDO_netlists
  -l
      Virtuoso library name.
  -r
      Reference netlist file list "STRING" ("CURRENTLY UNUSED).
  -s
      Simulator. "STRING". Default "eldoD"
  -t
      Create a subcircuit for the top-level netlist. Default off.
  -T
      Target name. The produced netlist will be names after this. Defaults to given cellname.
  -w 
      Working directory. Must contain cds.lib file
      and working environment (.cdsinit) for Virtuoso
      Default: current directory.
  -h
      Show this help.
EOF
}

WORKDIR=`pwd`
CELL=""
VIRTUOSOTARGETLIB=""
NETLIST_RUN_DIR=$WORKDIR/ELDO_simulation
SIMULATOR="eldoD"
SETTOPSUBCKT="0"
TOPSUBCKT=""
while getopts c:d:l:r:s:tT:w:h opt
do
  case "$opt" in
    c) CELL=${OPTARG};; 
    d) NETLIST_RUN_DIR=`readlink -f ${OPTARG}`;;
    l) VIRTUOSOTARGETLIB=${OPTARG};;
    r) REFLIBS=${OPTARG};;
    s) SIMULATOR=${OPTARG};;
    t) SETTOPSUBCKT="1";;
    T) TARGNAME=${OPTARG};;
    w) WORKDIR=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

if [ "${SETTOPSUBCKT}" == "1" ]; then
    if [ "${SIMULATOR}" == "eldoD" ]; then
        TOPSUBCKT="envOption('eldoSetTopLevelAsSubckt t)"
    elif [ "${SIMULATOR}" == "spectre" ]; then
        TOPSUBCKT="envOption('setTopLevelAsSubckt t)"
    fi
fi

mkdir -p $NETLIST_RUN_DIR

if [ -z "$VIRTUOSOTARGETLIB" ]; then
    echo "Virtuoso library not given"
    help_f;
    exit 1
fi
if [ -z "$CELL" ]; then
    echo "Cell not given library not given"
    help_f;
    exit 1
else
    if [ -z "$TARGNAME" ]; then 
        TARGNAME=${CELL}
    fi
    echo "Setting target name to ${TARGNAME}"
fi

NETLISTFILE=${NETLIST_RUN_DIR}/${CELL}/${SIMULATOR}/schematic/netlist/input.scs
NETLISTTARG=${NETLIST_RUN_DIR}/${CELL}/${SIMULATOR}/schematic/netlist/${TARGNAME}.scs

cd ${WORKDIR}
echo "Generating netlist..."
ocean --nograph << EOC
envSetVal("asimenv.startup" "simulator" 'string "${SIMULATOR}")
envSetVal("asimenv.startup" "projectDir" 'string "$NETLIST_RUN_DIR")
;;;;;Setting up the Artist Link MK 20120509;;;;
let((t_amsPath t_file)
    t_amsPath = getShellEnvVar("MGC_AMS_HOME")
    when(t_amsPath
    t_file = strcat(t_amsPath "/etc/cds/.cdsinit")
    when(isFile(t_file) load(t_file))
    )
)
simulator( '${SIMULATOR} )
modelFile()
design("$VIRTUOSOTARGETLIB" "$CELL" "schematic")
resultsDir( "$NETLIST_RUN_DIR" )
${TOPSUBCKT}
createNetlist(?recreateAll t ?display nil)
exit
EOC

echo "Moving output file from ${NETLISTFILE}"
echo "to ${NETLISTTARG}"
mv ${NETLISTFILE} ${NETLISTTARG}
exit 0
