#!/bin/sh
#############################################################################
# This is a script for mexporting DEF version of virtuoso cell.
#
# Created by Marko Kosunen on 04.03.2019 
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
cat << EOF
defout 1.0 (04.3.2019)
Export virtuoso cell as DEF
Written by Marko Pikkis Kosunen

SYNOPSIS
  defout [OPTIONS] 
DESCRIPTION
  Export virtuoso cell as DEF

  Please run this in CIW before the export 
    leSetAllLayerSelectable( t )
    geSelectAllFig()
    rteComposeTrunks()
    geDeselectAll()


OPTIONS
  -c
      Top cell name. STRING
  -f
     Output file, default ./<cell>.def 
  -l
      Virtuoso target library name. Copies the cell here prior export
  -o
      Origin offset STRING "X Y". Defines the offset of the cell contents 
      from the origin. Default "0 0"
  -s
      Virtuoso source library name. STRING
  -t
      Technology library.
  -h
      Show this help.
EOF
}

CELL=""
DEFFILE=""
VIRTUOSOTARGETLIB=""
LOCATION="0 0"

while getopts c:f:l:o:s:t:h opt
do
  case "$opt" in
    c) CELL=${OPTARG};; 
    f) DEFFILE=${OPTARG};;
    l) VIRTUOSOTARGETLIB=${OPTARG};;
    o) LOCATION=${OPTARG};;
    s) VIRTUOSOSOURCELIB=${OPTARG};;
    t) TECHLIB=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

if [ -z "$CELL" ]; then
    echo "Cell must be given"
    help_f;
    exit 0
fi

if [ -z "$VIRTUOSOSOURCELIB" ]; then
    echo "Source library must be given"
    help_f;
    exit 0
fi

if [ -z "$VIRTUOSOTARGETLIB" ]; then
    VIRTUOSOTARGETLIB="${VIRTUOSOSOURCELIB}_$(cat /dev/urandom | tr -dc _A-Z-a-z-0-9 | fold -w8 | head -n 1)"
    echo "Using temporary target library $VIRTUOSOTARGETLIB that will be delete after execution"
    DELTEMPLIB="1"

else
    echo "Will create target library $VIRTUOSOTARGETLIB that will NOT be delete after execution"
    DELTEMPLIB="0"
fi

if [ -z "${DEFFILE}" ]; then
    DEFFILE="./${CELL}.def"
fi

if [ ! -z "${LOCATION}" ]; then
    XP=$(echo ${LOCATION} | awk -F' ' '{print $1}')
    YP=$(echo ${LOCATION} | awk -F' ' '{print $2}')
    X=$(echo "${XP}*-1" | bc)
    Y=$(echo "${YP}*-1" | bc)
    echo "Content of the cell will be offset to $XP,$YP"
fi


DEFPATH=$(dirname ${DEFFILE})
if [ ! -d ${DEFPATH} ]; then 
    mkdir -p ${DEFPATH}
fi
VIRTUOSOSOURCELIB=$(basename $VIRTUOSOSOURCELIB) 



virtuoso -nograph << EOC
printf("Creating library in ${VIRTUOSOTARGETLIB}\n")
dbCreateLib( "${VIRTUOSOTARGETLIB}" "./" )
techBindTechFile(ddGetObj("${VIRTUOSOTARGETLIB}") "${TECHLIB}" "tech.db" t)
src=gdmCreateSpec( "${VIRTUOSOSOURCELIB}" "$CELL" "layout" "" "CDBA" )
dest=gdmCreateSpec( "${VIRTUOSOTARGETLIB}" "" "" "" "CDBA" )
skiplist=gdmCreateSpecList()

printf("Copying design ${CELL} to ${VIRTUOSOTARGETLIB}\n")
ccpCopyDesign( src dest t 'CCP_EXPAND_ALL skiplist nil nil "" "CDBA" 'CCP_UPDATE_COPIED_DATA )
cvNewInst = dbOpenCellViewByType("${VIRTUOSOTARGETLIB}" "${CELL}" "layout" "" "a")

; Flatten all instances
; flatten args:
; instId, levels, flatten_PCells, preservePins, preserveRODobjs, delDetachedBlockages, preservePinFlags, flattenVias
printf("Flattening instances\n")
foreach(inst cvNewInst~>instances 
    leFlattenInst(inst 32 nil t nil nil t t) 
);


; Move origin
printf("Moving origin\n")
leMoveCellViewOrigin(cvNewInst, ${X}:${Y})

; Extract connectivity
printf("Extracting connectivity\n")
lceExtract(cvNewInst)

printf("Saving design\n")
dbSave(cvNewInst)
dbClose(cvNewInst)
exit
EOC

#defout {-h | [-def] defFileName
#-lib libName
#-cell cellName
#-view viewName
#[-log logfileName]
#[-skipNoneOrViaCellType]
#[-mapDividerChar mapChar]
#[-dividerChar dividerChar]
#[-busbitChars busbitChars
#[-ver <5.4|5.5|5.6|5.7|5.8>]
#[-templateFile templateFileName]
#[-outputFloatingShapes]
#[-warnOnNotPlacedInsts]
#[-lockedColorsOnly]
#[-skipTrimShapes]
#[-skipTrimProductShapes]
#[-outputTrimmetalGapFill]
#[-outputTrimSegsAsNets]
#}
#Sourcelib should be defined in cds.lib

echo "Writing ${DEFFILE}"
rm -f ${DEFFILE}
defout -def ${DEFFILE} \
    -lib ${VIRTUOSOTARGETLIB} \
    -cell ${CELL} \
    -view layout \
    -ver 5.8 \
    -outputFloatingShapes

if [ ${DELTEMPLIB} == "1" ];then
virtuoso -nograph << EOC
    printf("Deleting library ${VIRTUOSOTARGETLIB}")
    ddDeleteObj(ddGetObj("${VIRTUOSOTARGETLIB}"))
    exit
EOC

fi
# Test the quality of DEF
TESTSTRING="$(sed -n '/^NETS/,/^END NETS/{//!p}'  "${DEFFILE}"  )"
if [ ! -z "${TESTSTRING}" ]; then
cat << EOF
WARNING: 
Your DEF file contains NETS section.
Nets not intented to be modified by automated routers 
should be in SPECIALNETS.

To solve this, execute in CIW the following commands:
leSeAllLayerSelectable( t )
geSelectAllFig()
rteComposeTrunks()
geDeselectAll()

Then save and re-run this script;
EOF

fi
exit 0

