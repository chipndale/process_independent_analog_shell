#!/bin/sh
#############################################################################
# This is a script formoving the origin of virtuoso cell.
#
# Created by Marko Kosunen on 20.11.2021
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
cat << EOF
move_origin 1.0 (20.11.2021)
Move origin of a virtuoso cell
Written by Marko Pikkis Kosunen

SYNOPSIS
  defout [OPTIONS] 
DESCRIPTION
  Export virtuoso cell as DEF

OPTIONS
  -c
      Top cell name. STRING
  -l
      Virtuoso library name.
  -o
      Origin offset STRING "X Y". Defines the offset of the cell contents 
      from the origin. Default "0 0"
  -h
      Show this help.
EOF
}

CELL=""
DEFFILE=""
VIRTUOSOTARGETLIB=""
LOCATION="0 0"

while getopts c:f:l:o:s:t:h opt
do
  case "$opt" in
    c) CELL=${OPTARG};; 
    l) VIRTUOSOTARGETLIB=${OPTARG};;
    o) LOCATION=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done

if [ -z "$CELL" ]; then
    echo "Cell must be given"
    help_f;
    exit 0
fi

if [ -z "$VIRTUOSOTARGETLIB" ]; then
    echo "Library must be given"
    help_f;
    exit 0
fi

if [ ! -z "${LOCATION}" ]; then
    LOC=($LOCATION)
    XP="${LOC[0]}"
    YP="${LOC[1]}" 
    X=$(echo "${XP}*-1" | bc)
    Y=$(echo "${YP}*-1" | bc)
    echo "Content of the cell will be offset to $XP,$YP"
fi

virtuoso -nograph << EOC
printf("Moving origin for ${CELL} in ${VIRTUOSOTARGETLIB}\n")
cvNewInst = dbOpenCellViewByType("${VIRTUOSOTARGETLIB}" "${CELL}" "layout" "" "a")
; Move origin
leMoveCellViewOrigin(cvNewInst, ${X}:${Y})

printf("Saving design\n")
dbSave(cvNewInst)
dbClose(cvNewInst)
exit
EOC
exit 0

