#!/bin/sh
#############################################################################
# This is a script for making a "release" of a Cadence virtuoso cell.
# The purpose is to compress all cell views also from the selected refefrence 
# libraries to a single bzip2 file
#
# Created by Marko Kosunen on 21.06.2018 (Finnish Midsummer eve)
# Last modification by Marko Kosunen, marko.kosunen@aalto.fi, 30.11.2020 22:13
#############################################################################
##Function to display help with -h argument and to control
##The configuration from the commnad line
help_f()
{
SCRIPTNAME="release_cell.sh"
cat << EOF
$(echo ${SCRIPTNAME} | tr [:upper:] [:lower:])- release_cell Release 1.0 (21.06.2018)
     $SCRIPTNAME-Compress a virtuoso cell views to a single bzip2 file"
     Written by Marko "Pikkis" Kosunen"

     SYNOPSIS"
       release_cell [OPTIONS] "
     DESCRIPTION"
       Compress a virtuoso cell views to a single bzip2 file"

     OPTIONS
       -c
          Top cell name. "STRING"
       -d
          Extract digital macro files (LEF, LIB, Verilog). Not extracted by default."
       -D
          Export DEF file. Not extracted by default."
       -f
          Force library owerwriting." 
       -G
          Ground net name list "STRING". Default "VSS"." 
       -l
          Virtuoso target library name (to be created in)  current directory. STRING"
       -O
          Overwrite the requirement of having VSS and VDD as the first entries of supply 
          and ground net lists.
       -r
          Library exclusion list "STRING".
       -s
          Virtuoso source library name. "STRING"
       -S
          Supply net name list "STRING". Default "VDD". 
       -t
          Technology library.
       -V
          Supply net voltage list. "v1 v2..." To override the default voltage for all supply nets, 
          use -V "0.88" . Otherwise the the length of the list must match the length of the 
          supply net list.
       -h
           Show this help.
EOF
}

CELL=""
VIRTUOSOTARGETLIB=""
REFLIBS=""
NETLIST=""
FORCE="0"
DIGIEXT="0"
DEFOUT="0"
GNDNETS="VSS"
SUPNETS="VDD"
VOLTAGES="0.9"
SUPNETSOVERRIDE=""
while getopts c:dDfG:l:Or:S:s:t:V:h opt
do
  case "$opt" in
    c) CELL=${OPTARG};; 
    d) DIGIEXT="1";;
    D) DEFOUT="1";;
    f) FORCE="1";;
    G) GNDNETS=${OPTARG};;
    l) VIRTUOSOTARGETLIB=${OPTARG};;
    O) SUPNETSOVERRIDE=" -O ";;
    r) REFLIBS=${OPTARG};;
    s) VIRTUOSOSOURCELIB=${OPTARG};;
    S) SUPNETS=${OPTARG};;
    V) VOLTAGES=${OPTARG};;
    t) TECHLIB=${OPTARG};;
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done
if [ -z "$TECHLIB" ]; then
    echo "Technology library not given"
    help_f;
    exit 1
fi

SUPARR=(${SUPNETS})
GNDARR=(${GNDNETS})
if [ -z "${SUPNETSOVERRIDE}" ]; then
    if [ "${SUPARR[0]}" != "VDD" ]; then
        cat << EOF  
"Primary supply net is not VDD but ${SUPARR[0]}.  This will most likely break the digital 
flow power domain configuration. Override with option -O if you are confident 
about what you are doing.
EOF
        exit 1
    fi
    if [ "${GNDARR[0]}" != "VSS" ]; then
        cat << EOF  
"Primary ground net is not VSS but ${GNDARR[0]}.  This will most likely break the digital 
flow power domain configuration. Override with option -O if you are confident 
about what you are doing.
EOF
        exit 1
    fi
fi

VIRTUOSOTARGETPATH=$(dirname $VIRTUOSOTARGETLIB)
VIRTUOSOTARGETLIBNAME=$(basename $VIRTUOSOTARGETLIB)

#Sourcelib should be defined in cds.lib
VIRTUOSOSOURCELIB=$(basename $VIRTUOSOSOURCELIB) 

if [ ! -d ${VIRTUOSOSOURCELIB} ]; then
    echo "Virtuoso source library ${VIRTUOSOSOURCELIB} doesn't exist!"
    exit 1
fi

if [ "${FORCE}" == '1' ]; then
    DEL="ddDeleteObj(ddGetObj(\""${VIRTUOSOTARGETLIBNAME}"\"))"
elif [ -d "${VIRTUOSOTARGETLIB}" ]; then
    echo "Library ${VIRTUOSOTARGETLIB} exists. Force delete with option -f."
    exit 1;
fi

#Parse the reflibs list to Skill format
RFSTR="list( "
for lib in $REFLIBS; do
    RFSTR="$RFSTR \"$lib\""
done
RFSTR="$RFSTR )"
# Create back up of working copy
echo "Backing up working copy to library ${VIRTUOSOSOURCELIB}_bu" 
virtuoso -nograph << EOC
src_schematic=gdmCreateSpec( "${VIRTUOSOSOURCELIB}" "$CELL" "schematic" "" "CDBA" )
src_layout=gdmCreateSpec( "${VIRTUOSOSOURCELIB}" "$CELL" "layout" "" "CDBA" )
src_symbol=gdmCreateSpec( "${VIRTUOSOSOURCELIB}" "$CELL" "symbol" "" "CDBA" )
src_auCdl=gdmCreateSpec( "${VIRTUOSOSOURCELIB}" "$CELL" "auCdl" "" "CDBA" )
dest=gdmCreateSpec( "${VIRTUOSOSOURCELIB}_bu" "" "" "" "CDBA" )
reflibs = $RFSTR
skiplist=gdmCreateSpecList()
foreach(lib reflibs \
    lib=gdmCreateSpec( lib "" "" "" "CDBA" ) \
    gdmAddSpecToSpecList(lib skiplist) \
)
viewlist=list("schematic" "layout" "symbol" "auCdl")
ccpCopyExactDesign( src_schematic dest t 'CCP_EXPAND_ALL skiplist nil viewlist "" "CDBA" 'CCP_UPDATE_COPIED_DATA ) 
ccpCopyExactDesign( src_layout dest t 'CCP_EXPAND_ALL skiplist nil viewlist "" "CDBA" 'CCP_UPDATE_COPIED_DATA ) 
ccpCopyExactDesign( src_symbol dest t 'CCP_EXPAND_ALL skiplist nil viewlist "" "CDBA" 'CCP_UPDATE_COPIED_DATA ) 
ccpCopyExactDesign( src_auCdl dest t 'CCP_EXPAND_ALL skiplist nil viewlist "" "CDBA" 'CCP_UPDATE_COPIED_DATA ) 
exit

EOC

# Generating Verilog, LEF and LIB files needed by digital tools
if [ $DIGIEXT == "1" ]; then
    if [ $FORCE == "1" ]; then
        ./shell/generate_verilog.sh -c ${CELL} -l ${VIRTUOSOSOURCELIB} -f && \
        ./shell/generate_lef.sh -c ${CELL} -l ${VIRTUOSOSOURCELIB} -f -S"${SUPNETS}" -G"${GNDNETS}" && \
        ./shell/generate_lib.sh -c ${CELL} -l ${VIRTUOSOSOURCELIB} \
            -f -S"${SUPNETS}" -G"${GNDNETS}" ${SUPNETSOVERRIDE}

    else
        ./shell/generate_verilog.sh -c ${CELL} -l ${VIRTUOSOSOURCELIB} && \
        ./shell/generate_lef.sh -c ${CELL} -l ${VIRTUOSOSOURCELIB} -S"${SUPNETS}" -G"${GNDNETS}" && \
        ./shell/generate_lib.sh -c ${CELL} -l ${VIRTUOSOSOURCELIB} \
            -f -S"${SUPNETS}" -G"${GNDNETS}" ${SUPNETSOVERRIDE}
    fi
fi

if [  "$?" != "0" ]; then
    echo "ERROR in release_cell.sh"
fi

if [ $DEFOUT == "1" ]; then
    ./shell/defout.sh -c ${CELL} -s ${VIRTUOSOSOURCELIB} -t${TECHLIB}
fi

#Create the library
virtuoso -nograph << EOC 
    $DEL
    dbCreateLib("${VIRTUOSOTARGETLIBNAME}" "./") 
    techBindTechFile(ddGetObj("${VIRTUOSOTARGETLIBNAME}") "${TECHLIB}" "tech.db" t)
    exit
EOC

# Moving Verilog, LEF and LIB files to the release library
if [ $DIGIEXT == "1" ]; then
    if [ -d "${CELL}_lef" ]; then
        if [ -f "${CELL}_lef/${CELL}.lef" ]; then
            mkdir -p "${VIRTUOSOTARGETPATH}/${VIRTUOSOTARGETLIB}/digital/lef"
            cp ${CELL}_lef/${CELL}.lef ${VIRTUOSOTARGETPATH}/${VIRTUOSOTARGETLIB}/digital/lef
        fi
        if [ -f "${CELL}_lef/${CELL}.v" ]; then
            mkdir -p "${VIRTUOSOTARGETPATH}/${VIRTUOSOTARGETLIB}/digital/verilog"
            cp ${CELL}_lef/${CELL}.v ${VIRTUOSOTARGETPATH}/${VIRTUOSOTARGETLIB}/digital/verilog
        fi
    fi
    if [ -d "${CELL}_lib" ]; then
        mkdir -p "${VIRTUOSOTARGETPATH}/${VIRTUOSOTARGETLIB}/digital/lib"
        cp ${CELL}_lib/*.lib ${VIRTUOSOTARGETPATH}/${VIRTUOSOTARGETLIB}/digital/lib
    fi
fi
if [ -f "./${CELL}.def" ]; then
    mkdir -p "${VIRTUOSOTARGETPATH}/${VIRTUOSOTARGETLIB}/digital/def"
    mv ./${CELL}.def ${VIRTUOSOTARGETPATH}/${VIRTUOSOTARGETLIB}/digital/def
fi

#Copy the library 
CDS_COPY_AUTO_RENAME=yes
export CDS_COPY_AUTO_RENAME

# Only the views schematic, layout, symbol and auCdl are released
virtuoso -nograph << EOC
src_schematic=gdmCreateSpec( "${VIRTUOSOSOURCELIB}" "$CELL" "schematic" "" "CDBA" )
src_layout=gdmCreateSpec( "${VIRTUOSOSOURCELIB}" "$CELL" "layout" "" "CDBA" )
src_symbol=gdmCreateSpec( "${VIRTUOSOSOURCELIB}" "$CELL" "symbol" "" "CDBA" )
src_auCdl=gdmCreateSpec( "${VIRTUOSOSOURCELIB}" "$CELL" "auCdl" "" "CDBA" )
dest=gdmCreateSpec( "${VIRTUOSOTARGETLIBNAME}" "" "" "" "CDBA" )
reflibs = $RFSTR
skiplist=gdmCreateSpecList()
foreach(lib reflibs \
    lib=gdmCreateSpec( lib "" "" "" "CDBA" ) \
    gdmAddSpecToSpecList(lib skiplist) \
)
viewlist=list("schematic" "layout" "symbol" "auCdl")
ccpCopyExactDesign( src_schematic dest t 'CCP_EXPAND_ALL skiplist nil viewlist "" "CDBA" 'CCP_UPDATE_COPIED_DATA ) 
ccpCopyExactDesign( src_layout dest t 'CCP_EXPAND_ALL skiplist nil viewlist "" "CDBA" 'CCP_UPDATE_COPIED_DATA ) 
ccpCopyExactDesign( src_symbol dest t 'CCP_EXPAND_ALL skiplist nil viewlist "" "CDBA" 'CCP_UPDATE_COPIED_DATA ) 
ccpCopyExactDesign( src_auCdl dest t 'CCP_EXPAND_ALL skiplist nil viewlist "" "CDBA" 'CCP_UPDATE_COPIED_DATA ) 
exit

EOC

unset CDS_COPY_AUTO_RENAME

#Creating the package for distribution
echo "Deleting lock files from  ${VIRTUOSOTARGETLIB}"
find ${VIRTUOSOTARGETLIB} -type f -name \*.cdslck* -exec rm -f {} \; 


echo "Creating package ${VIRTUOSOTARGETLIB}.tbz2"
rm -f  ${VIRTUOSOTARGETLIB}.tbz2
tar cjf ${VIRTUOSOTARGETLIB}.tbz2 ${VIRTUOSOTARGETLIB}
exit 0

